﻿using System.Collections;
using System.Collections.Generic;
using Tictech.EnterpriseUniversity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.Management;

namespace UnityEngine.XR.ARFoundation.Samples
{
    public class BackButton : MonoBehaviour
    {
        [SerializeField]
        GameObject m_BackButton;
        public GameObject backButton
        {
            get { return m_BackButton; }
            set { m_BackButton = value; }
        }

        void Start()
        {
            if (m_BackButton)
                if (Application.CanStreamedLevelBeLoaded("Main Menu"))
                {
                    m_BackButton.SetActive(true);
                }
        }

        public void BackButtonPressed()
        {
            SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
            /*if(EuRuntimeManager.Instance)
                SceneManager.MoveGameObjectToScene(EuRuntimeManager.Instance.gameObject, SceneManager.GetSceneByName("Main Menu"));*/
            LoaderUtility.Deinitialize();
        }
    }
}
