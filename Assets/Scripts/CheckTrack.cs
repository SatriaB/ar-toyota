using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class CheckTrack : MonoBehaviour
{
    ARTrackedImageManager imageTracker;
    TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        imageTracker = GameObject.FindObjectOfType<ARTrackedImageManager>();
        imageTracker.trackedImagesChanged += ImageChangedHandle;
        if (GetComponent<TextMeshProUGUI>())
            text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [SerializeField]
    ARTrackedImageManager m_TrackedImageManager;

    void OnEnable() => m_TrackedImageManager.trackedImagesChanged += OnChanged;

    void OnDisable() => m_TrackedImageManager.trackedImagesChanged -= OnChanged;

    void OnChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var newImage in eventArgs.added)
        {
            // Handle added event
            text.text = "Nongol : baru";
        }

        foreach (var updatedImage in eventArgs.updated)
        {
            // Handle updated event
            text.text = "Nongol : ganti";
        }

        foreach (var removedImage in eventArgs.removed)
        {
            // Handle removed event
            text.text = "Nongol : ilang";
        }
    }

    // Method handling found/lost tracked images.
    void ImageChangedHandle(ARTrackedImagesChangedEventArgs imgChangedArgs)
    {
        // loop over new found images. imgChangedArgs.added is List<ARTrackedImage>
        foreach (var item in imgChangedArgs.added)
        {
            var go = item.gameObject;
        }
    }
}
