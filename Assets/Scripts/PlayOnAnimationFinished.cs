using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnFinished();

[System.Serializable]
public struct Reset
{
    public GameObject showObject;
    public Vector3 defaultPosition;
    public Vector3 defaultRotation;
}

public class PlayOnAnimationFinished : MonoBehaviour
{
    Animator thisAnimator;
    public Animator targetAnimator;
    public Animation targetAnimation;
    public string animatorStateName;

    public bool triggerOnce;
    bool triggered = false;

    public event OnFinished onFinished;

    [SerializeField]
    public Reset[] displayedObjectsOnFinish;

    private void Start()
    {
        thisAnimator = GetComponent<Animator>();
    }

    public void PlayNext()
    {
        Debug.Log(gameObject.transform.parent.name + " finished");
            if (targetAnimation != null)
            {
                if (!targetAnimation.isPlaying)
                {
                    if (animatorStateName != null)
                    {
                        targetAnimation.Play();
                    }
                }
            }
            
            if(targetAnimator!=null)
            {
                if (triggerOnce)
                {
                    if (!triggered)
                    {
                        targetAnimator.Play(animatorStateName);
                        triggered = true;
                    }
                }
                else
                {
                    targetAnimator.Play(animatorStateName);
                }
            }

            if (displayedObjectsOnFinish.Length > 0)
            {
                for (int i = 0; i < displayedObjectsOnFinish.Length; i++)
                {
                    displayedObjectsOnFinish[i].showObject.transform.localPosition = displayedObjectsOnFinish[i].defaultPosition;
                    displayedObjectsOnFinish[i].showObject.transform.localEulerAngles = displayedObjectsOnFinish[i].defaultRotation;
                    displayedObjectsOnFinish[i].showObject.SetActive(true);
                }
            }
    }


}
