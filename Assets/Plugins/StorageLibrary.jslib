mergeInto(LibraryManager.library, {

  Call: function () {
    window.alert("Hello, world!");
  },

  CallString: function (str) {
    window.alert(Pointer_stringify(str));
  },

  PrintFloatArray: function (array, size) {
    for(var i = 0; i < size; i++)
    console.log(HEAPF32[(array >> 2) + i]);
  },

  getToken: function (str0) {
    var token1 = localStorage.getItem(Pointer_stringify(str0)); 
    //window.alert("get token : " + token1);
    
    var bufferSize = lengthBytesUTF8(token1) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(token1, buffer, bufferSize);
    return buffer;
    
    //return token1;
  },

  setToken: function (str1, str2) {
    localStorage.setItem(Pointer_stringify(str1), Pointer_stringify(str2)); 
  },

  AddNumbers: function (x, y) {
    return x + y;
  },

  StringReturnValueFunction: function () {
    var returnStr = "bla";
    var bufferSize = lengthBytesUTF8(returnStr) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(returnStr, buffer, bufferSize);
    return buffer;
  },

  BindWebGLTexture: function (texture) {
    GLctx.bindTexture(GLctx.TEXTURE_2D, GL.textures[texture]);
  },

});