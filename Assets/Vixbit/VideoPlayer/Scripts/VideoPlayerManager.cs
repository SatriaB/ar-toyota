using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoPlayerManager : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public string streamingAssetsVideoURL;
    [ReadOnly]
    public int clipIndex = 0;
    public List<VideoClip> Clips;

    bool isPlaying = false;
    bool canChange;
    bool fullScreen = false;

    [Header("UI")]
    public GameObject nonFullScreenVideo;
    public GameObject fullScreenVideo;
    public Button fullScreenPlayPauseButton;
    public Button playPauseButton;
    public Button prevIndex, nextIndex;
    public Sprite playIcon;
    public Sprite pauseIcon;
    public Slider fullScreenVideoPlayerProgressBar;
    public Slider videoPlayerProgressBar;

    private void Start()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        if(!string.IsNullOrEmpty(streamingAssetsVideoURL))
        videoPlayer.url = Application.streamingAssetsPath + "/" + streamingAssetsVideoURL;
        videoPlayer.targetTexture.Release();

        playPauseButton.onClick.AddListener(() =>
        {
            PlayPause();
        });

        fullScreenPlayPauseButton.onClick.AddListener(() =>
        {
            PlayPause();
        });

        videoPlayerProgressBar.onValueChanged.AddListener((value) =>
        {
            if (canChange)
            {
                videoPlayer.frame = (long)(videoPlayer.frameCount * value);
            }
        });

        fullScreenVideoPlayerProgressBar.onValueChanged.AddListener((value) =>
        {
            if (canChange)
            {
                videoPlayer.frame = (long)(videoPlayer.frameCount * value);
            }
        });

        videoPlayer.loopPointReached += (vp) =>
        {
            vp.targetTexture.Release();
        };

        videoPlayerProgressBar.interactable = false;
        fullScreenVideoPlayerProgressBar.interactable = false;

        SetPlayPause(isPlaying);
        checkIndex();
    }

    void checkIndex()
    {
        if (prevIndex)
        {
            if (clipIndex <= 0)
                prevIndex.interactable = false;
            else prevIndex.interactable = true;
            prevIndex.onClick.AddListener(() => PrepareClip(-1));
        }
        if (nextIndex)
        {
            if (clipIndex >= Clips.Count - 1)
                nextIndex.interactable = false;
            else nextIndex.interactable = true;
            nextIndex.onClick.AddListener(() => PrepareClip(1));
        }
    }

    public void CanChange(bool value)
    {
        canChange = value;
        if(value)
        {
            videoPlayer.Pause();
        }
        else
        {
            if (isPlaying)
            {
                videoPlayer.Play();
            }
        }
        videoPlayer.frame = (long)((fullScreen ? fullScreenVideoPlayerProgressBar.value : videoPlayerProgressBar.value) * videoPlayer.frameCount);

        
    }

    public void PlayPause()
    {
        if(!videoPlayer.isPrepared)
        {
            videoPlayer.Prepare();
            videoPlayerProgressBar.interactable = true;
            fullScreenVideoPlayerProgressBar.interactable = true;
        }

        isPlaying = !isPlaying;

        if(isPlaying)
        {
            videoPlayer.Play();
        }
        else
        {
            videoPlayer.Pause();
        }

        SetPlayPause(isPlaying);
    }

    void SetPlayPause(bool value)
    {
        playPauseButton.image.sprite = value ? pauseIcon : playIcon;
        fullScreenPlayPauseButton.image.sprite = value ? pauseIcon : playIcon;
    }

    private void Update()
    {
        if (isPlaying && videoPlayer.isPrepared)
        {
            if (!canChange)
            {
                videoPlayerProgressBar.value = (float)videoPlayer.frame / videoPlayer.frameCount;
                fullScreenVideoPlayerProgressBar.value = (float)videoPlayer.frame / videoPlayer.frameCount;
            }
        }
    }

    public void PrepareClip(int index)
    {
        clipIndex = Mathf.Clamp(clipIndex + index, 0, Clips.Count - 1);
        StartCoroutine(clipsPreparation(clipIndex));
    }

    public void FullScreen(bool value)
    {
        fullScreen = value;
        nonFullScreenVideo.SetActive(!value);
        fullScreenVideo.SetActive(value);

        if (fullScreen)
        {
            fullScreenVideoPlayerProgressBar.value = videoPlayerProgressBar.value;
        }
        else
        {
            videoPlayerProgressBar.value = fullScreenVideoPlayerProgressBar.value;
        }
    }

    IEnumerator clipsPreparation(int index)
    {
        videoPlayer.enabled = false;
        yield return new WaitForSeconds(.1f);
        videoPlayerProgressBar.value = 0;
        fullScreenVideoPlayerProgressBar.value = 0;
        SetPlayPause(false);
        isPlaying = false;
        checkIndex();
        if (GetComponent<VideoPlayer>())
        {
            VideoPlayer vp = GetComponent<VideoPlayer>();
            vp.clip = Clips[index];
            vp.targetTexture.Release();
        }
        videoPlayer.enabled = true;
    }
}
