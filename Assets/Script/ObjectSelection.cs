using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ObjectSelection : MonoBehaviour
{
    public string layerName = "Default";
    Outline ol;
    public bool selectable = true;
    [ReadOnly]
    public bool preview = false;
    public float previewSize = 1;
    public Quaternion rotateBaseBy;
    public Vector3 offset;
    public GameObject duplicate { get; set; }
    ARRaycastManager arRaycast;
    static List<ARRaycastHit> arHit = new List<ARRaycastHit>();
    /*private void OnMouseDown()
    {
         ol = gameObject.AddComponent<Outline>();
    }*/
    public ObjectSelection(string baseLayer, float basePreviewSize)
    {
        layerName = baseLayer;
        previewSize = basePreviewSize;
    }

    private void Start()
    {
        gameObject.layer = LayerMask.NameToLayer(layerName);
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (EventsControl.Instance)
                if (EventsControl.Instance.onPreview)
                    return;
            if (!arRaycast)
            {
                if (GameObject.FindObjectOfType<ARRaycastManager>())
                    arRaycast = GameObject.FindObjectOfType<ARRaycastManager>();
            }

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // Does the ray intersect any objects excluding the player layer

            if (Physics.Raycast(ray, out hit/*, Mathf.Infinity, LayerMask.NameToLayer(layerName)*/) /*||
                arRaycast.Raycast(Input.mousePosition, arHit, TrackableType.All)*/)
            {
                if (!EventsControl.Instance)
                {
                    Debug.LogError("No Events Control Found");
                    return;
                }
                if (EventsControl.Instance.onPreview || EventsControl.Instance.lerping || EventsControl.Instance.assemblyState == EventsControl.AssemblyState.Assembled)
                    return;
                Rigidbody rb = hit.collider.GetComponentInParent<Rigidbody>();
                GameObject rbo = null;
                if (rb)
                    if (rb.gameObject == gameObject)
                        rbo = gameObject;
                if (hit.collider.gameObject == gameObject ||
                    rbo != null/*|| arHit[0].trackable.gameObject == gameObject*/)
                {
                    Debug.Log("Hit : " + hit.collider.gameObject);
                    if (!gameObject.GetComponent<Outline>())
                        ol = gameObject.AddComponent<Outline>();
                    else ol = gameObject.GetComponent<Outline>();
                    ol.enabled = true;
                    if (selectable)
                    {
                        preview = true;
                        if (EventsControl.Instance)
                        {
                            EventsControl.Instance.selectedObject = this;
                            EventsControl.Instance.generalInfoButton.SetActive(false);
                            EventsControl.Instance.growIn(EventsControl.Instance.previewPanel.gameObject);
                            if (hit.collider.GetComponentInChildren<InfoPoint>())
                            {
                                EventsControl.Instance.previewPanel.gameObject.SetActive(true);
                                hit.collider.GetComponentInChildren<InfoPoint>().setContent();
                                EventsControl.Instance.onPreview = true;
                                if (GameObject.Find("SwitchButton"))
                                    GameObject.Find("SwitchButton").SetActive(false);
                                //EventsControl.Instance.growIn(EventsControl.Instance.partInfoPanel.gameObject);
                            }
                        }
                    }
                }
                else
                {
                    if (ol)
                        if (ol.enabled)
                            ol.enabled = false;
                }
            }
            else if (EventsControl.Instance && ol)
                if (ol.enabled && !EventsControl.Instance.onPreview)
                {
                    ol.enabled = false;
                    preview = false;
                }
        }
    }

    private void FixedUpdate()
    {
        if (EventsControl.Instance)
            if (EventsControl.Instance.selectedObject != this)
                preview = false;

        if (preview)
        {
            if (!duplicate)
            {
                if (EventsControl.Instance)
                {
                    /*if (EventsControl.Instance.onPreview)
                        return;*/
                    if (EventsControl.Instance.previewCamera)
                    {
                        if (EventsControl.Instance.previewCamera.GetComponentsInChildren<Transform>().Length > 0)
                        {
                            GameObject baseObject = EventsControl.Instance.previewCamera.transform.GetChild(0).Find("Base").gameObject;
                            baseObject.GetComponentsInChildren<Transform>().ForEach(x =>
                            {
                                if (x != baseObject.transform)
                                {
                                    Destroy(x.gameObject);
                                }
                            });
                        }

                        if (EventsControl.Instance.previewCamera.transform.GetChild(0).Find("Base").transform)
                        {
                            duplicate = Instantiate(gameObject, GameObject.Find("CameraPreview").transform.GetChild(0).GetChild(0));
                            //duplicate.transform.localRotation = Quaternion.identity;
                            duplicate.transform.localRotation = rotateBaseBy;
                            Destroy(duplicate.GetComponent<ObjectSelection>());
                            duplicate.transform.localPosition = new Vector3(offset.x, offset.y, /*duplicate.transform.localPosition.z + */offset.z);
                            EventsControl.Instance.onPreview = true;
                            duplicate.AddComponent<RotationControl>();
                            if (EventsControl.Instance.previewCamera.GetComponentInChildren<Camera>())
                            {
                                EventsControl.Instance.previewCamera.GetComponentInChildren<Camera>().orthographicSize = previewSize;
                                EventsControl.Instance.previewCamera.GetComponentInChildren<Camera>().fieldOfView = previewSize;
                            }
                        }
                        else Debug.LogError("No base transform for camera");
                    }
                    else Debug.LogError("No camera assigned");
                }
                else Debug.LogError("No events control instance");
            }
            /*else
            {
                if (EventsControl.Instance)
                    if (EventsControl.Instance.onPreview)
                        return;
                duplicate.GetComponent<InfoPoint>().setContent();
            }*/
        }
    }
}
