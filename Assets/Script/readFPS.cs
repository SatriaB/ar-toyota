using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class readFPS : MonoBehaviour
{
    private float fps = 30f;
    public float currentFPS;
    bool DrawGui = false;

    private void OnDisable()
    {
        DrawGui = false;
    }

    private void OnEnable()
    {
        DrawGui = true;
    }

    void OnGUI()
    {
        if (!DrawGui)
        {
            GUI.Label(new Rect(0, 0, 100, 100), "FPS: no FPS counter");
            return;
        }
        Application.targetFrameRate = 60;
        float newFPS = 1.0f / Time.smoothDeltaTime;
        fps = Mathf.Lerp(fps, newFPS, 0.0005f);
        GUI.Label(new Rect(0, 0, 100, 100), "FPS: " + ((int)fps).ToString() + " - " + gameObject.GetInstanceID().ToString());
        currentFPS = (int)fps;
    }
}
