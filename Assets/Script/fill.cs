using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class fill : MonoBehaviour
{
#if UNITY_EDITOR
    [Button]
    public void FillDown()
    {
        foreach (var infoPoint in GetComponentsInChildren<InfoPoint>(true))
        {
            foreach (var information in infoPoint.informations)
            {
                if (information.fungsi.details.Count == 0 && !string.IsNullOrEmpty(information.fungsi.detail))
                {
                    information.fungsi.details.Add(information.fungsi.detail);
                }
                if (information.abnormalityEffect.details.Count == 0 && !string.IsNullOrEmpty(information.abnormalityEffect.detail))
                {
                    information.abnormalityEffect.details.Add(information.abnormalityEffect.detail);
                }
            }
            EditorUtility.SetDirty(infoPoint);
            Debug.Log("Set : " + infoPoint);
        }
    }

    [Button]
    public void FillEmision()
    {
        foreach (var item in GetComponentsInChildren<MeshRenderer>())
        {
            item.sharedMaterial.SetTexture("_EmissionMap", item.material.GetTexture("_MainTex"));
            item.sharedMaterial.SetColor("_EmissionColor", Color.white);
        }
    }

    [Button]
    public void EmisionOff()
    {
        foreach (var item in GetComponentsInChildren<MeshRenderer>(true))
        {
            item.sharedMaterial.DisableKeyword("_EMISSION");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
#endif
}
