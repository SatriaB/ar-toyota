﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class StorageInteraction : MonoBehaviour
{
    /*[DllImport("__Internal")]
    public static extern string getToken(string str0);*/

#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void Call();

    [DllImport("__Internal")]
    public static extern void setToken(string str1, string str2);

    [DllImport("__Internal")]
    public static extern string getToken(string str0);

    [DllImport("__Internal")]
    private static extern void CallString(string str);

    [DllImport("__Internal")]
    private static extern void PrintFloatArray(float[] array, int size);

    [DllImport("__Internal")]
    private static extern int AddNumbers(int x, int y);

    [DllImport("__Internal")]
    private static extern string StringReturnValueFunction();

    [DllImport("__Internal")]
    private static extern void BindWebGLTexture(int texture);
#endif

    void Start()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        /*Call();

        CallString("This is a string.");

        float[] myArray = new float[10];
        PrintFloatArray(myArray, myArray.Length);

        int result = AddNumbers(5, 7);
        Debug.Log(result);

        Debug.Log(StringReturnValueFunction());

        var texture = new Texture2D(0, 0, TextureFormat.ARGB32, false);
        BindWebGLTexture(texture.GetNativeTextureID());*/
        //setToken("auth._token.local1", "asu");
        /*Debug.LogError("token set : " + "asu");
        Debug.LogError("token get : " + getToken("auth._token.local1"));*/
        Debug.LogError("token get : " + getToken("auth._token.local1"));
#endif

        /*Debug.LogError(getToken());*/
    }
}
