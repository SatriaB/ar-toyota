using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

//It is common to create a class to contain all of your
//extension methods. This class must be static.
public static class ExtensionMethod
{
    //Even though they are used like normal methods, extension
    //methods must be declared static. Notice that the first
    //parameter has the 'this' keyword followed by a Transform
    //variable. This variable denotes which class the extension
    //method becomes a part of.
    public static void ResetTransformation(this Transform trans)
    {
        trans.position = Vector3.zero;
        trans.localRotation = Quaternion.identity;
        trans.localScale = new Vector3(1, 1, 1);
    }

    public static GameObject GetObjectByName(this GameObject baseObject, string objectName)
    {
        if (baseObject.GetComponentsInChildren<Transform>(true).FirstOrDefault(x => x.name.Contains(objectName)))
            return (baseObject.GetComponentsInChildren<Transform>(true).FirstOrDefault(x => x.name.Contains(objectName)).gameObject);
        return default;
    }

    public static T GetComponentByName<T>(this GameObject baseObject, string objectName)
    {
        if (baseObject.GetComponentsInChildren<Transform>(true).FirstOrDefault(x => x.name.Contains(objectName)))
            return baseObject.GetComponentsInChildren<Transform>(true).FirstOrDefault(x => x.name.Contains(objectName)).GetComponentInChildren<T>();
        return default;
    }

    public static Image CopyImage(this Image im, Image target)
    {
        im.sprite = target.sprite;
        return default;
    }
}