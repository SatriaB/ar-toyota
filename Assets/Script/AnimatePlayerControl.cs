using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimatePlayerControl : MonoBehaviour
{
    [System.Serializable]
    public class AnimationIndex
    {
        public string id;
        public string animationName;
    }

    public List<AnimationIndex> animate;
    public UnityEvent OnStartPlay, OnEndPlay;

    public void Animate(string id)
    {
        bool exist = false;
        if (animate.Exists(x => x.id == id) && GameObject.FindGameObjectWithTag("AR"))
        {
            string _name = animate.Find(x => x.id == id).animationName;
            Animator animator = GameObject.FindGameObjectWithTag("AR").GetComponentInChildren<Animator>(true);
            Debug.Log(GameObject.FindGameObjectWithTag("AR").GetComponentInChildren<Animator>(true));
            Debug.Log("animator : " + animator + "\n play : " + _name);
            /*foreach (var item in animator.runtimeAnimatorController.animationClips)
            {
                if (item.name == _name)
                {
                    exist = true;
                    break;
                }
            }
            if (exist)*/
                animator.Play(_name);
            Timing.RunCoroutine(checkPlaying(animator));
            //else Debug.LogError("Animation state doesnt exist");
        }
        else
        {
            Debug.LogError("No animation or animator found");
        }
    }

    IEnumerator<float> checkPlaying(Animator animator)
    {
        if (EventsControl.Instance)
            EventsControl.Instance.lerping = true;
        OnStartPlay.Invoke();
        Debug.Log("Start Playing");
        yield return Timing.WaitForOneFrame;
        while (!animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            //Debug.Log("Playing : " + animator.GetCurrentAnimatorStateInfo(0));
            yield return Timing.WaitForOneFrame;
        }
        Debug.Log("End Playing");
        OnEndPlay.Invoke();
        if (EventsControl.Instance)
            EventsControl.Instance.lerping = false;
    }

    public void assembleState(string state)
    {
        if (EventsControl.Instance)
            EventsControl.Instance.assembleState(state);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
