using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{
    public RectTransform listPanel;
    public GameObject listPrefab;
    public RawImage previewPanel;
    public TextMeshProUGUI title;
    public Color baseColor, selectedColor;
    
    [System.Serializable]
    public class InfoTemplate
    {
        public string id;
        public RectTransform basePanel, infoArea;
        public GameObject template;
        public bool turnOffPrefab = true;
    }

    public List<InfoTemplate> info;
    [ReadOnly]
    public List<GameObject> objectList, subObjectList;

    public InfoTemplate getInfo(string id)
    {
        return info.Find(x => x.id == id);
    }

    private void Start()
    {
        foreach (var item in info)
        {
            if (item.turnOffPrefab)
            {
                item.template.gameObject.SetActive(false);
            }
        }
    }
    private void FixedUpdate()
    {
        if (previewPanel)
            if (previewPanel.rectTransform.sizeDelta.x != previewPanel.rectTransform.sizeDelta.y)
            {
                previewPanel.rectTransform.sizeDelta = new Vector2(previewPanel.rectTransform.sizeDelta.x, previewPanel.rectTransform.sizeDelta.x);
            }
    }

    public void setZoomPreview(string id)
    {
        if (EventsControl.Instance)
        {
            EventsControl.Instance.zoomPreview.sprite = getInfo(id).infoArea.GetComponentInChildren<Image>().sprite;
        }
    }
}
