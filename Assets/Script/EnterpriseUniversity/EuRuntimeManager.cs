﻿using System.Collections.Generic;
using System.Xml;
using MEC;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Networking;
//using Tictech.Oculus.Scenario;

namespace Tictech.EnterpriseUniversity
{
    using ResourceManager;
    using TMPro;
    using UnityEngine.SceneManagement;

    public class EuRuntimeManager : MonoBehaviour
    {
        public bool UILog = false;
        public string currentSceneName { get; set; }
        public static EuRuntimeManager Instance;
        public bool guestUser = false;
        public bool loggingIn = false;
        [SerializeField]
        public List<Scores> scores = new List<Scores>();
        [System.Serializable]
        public class Scores
        {
            public int id;
            public string sceneName;
        }

        [Header("API Access")]
        public string baseUrl;
        public string loginHandle;
        public string usersHandle;
        public string scoreHandle;
        public string scoreUserParameter;

#if UNITY_EDITOR
        [Header("Debug")]
        public string debugUsername;
        public string debugPassword;
        public int debugTrainingId;
        public string debugType;

        public int debugScore;

        [Button]
        void TestPostScore()
        {
            Timing.RunCoroutine(PostScore(debugTrainingId, debugScore, debugType));
        }

        /*private void FixedUpdate()
        {
            WriteLog("Update : " + Time.realtimeSinceStartup);
            if (SceneManager.GetActiveScene().name != currentSceneName)
            {
                if (guestUser || User == null)
                    return;
                WriteLog("Loading scene : " + SceneManager.GetActiveScene().path);
                string sceneName = SceneManager.GetActiveScene().name;
                if (scores.Exists(x => x.sceneName == sceneName))
                    Timing.RunCoroutine(PostScore(scores.Find(x => x.sceneName == sceneName).id, 100, ""));
                currentSceneName = SceneManager.GetActiveScene().name;
            }
        }*/

        public string debugEmail, debugPhone;
        [Button]
        void TestGuestPost()
        {
            Timing.RunCoroutine(PostGuest(debugEmail, debugPhone));
        }
        #endif

        [SerializeField, ReadOnly]
        public EuUserData User { get; private set; }
        [SerializeField, ReadOnly]
        public EuUserScoreData[] UserScores { get; private set; }
        StorageInteraction storage;
        public void SignOutUser()
        {
            User = null;
        }

        private void Awake()
        {
            if (!UILog)
                GetComponentInChildren<Canvas>(true).gameObject.SetActive(false);
            else GetComponentInChildren<Canvas>(true).gameObject.SetActive(true);
            if (Instance == null)
            {
                Instance = this;
                if (GetComponent<StorageInteraction>())
                    storage = GetComponent<StorageInteraction>();
                else storage = gameObject.AddComponent<StorageInteraction>();
                DontDestroyOnLoad(this);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }

        }

        public IEnumerator<float> SignIn(string username, string password)
        {
            EuResponse authStatus = null;

            loggingIn = true;
            //var pc = PersistentCanvas.Instance;
            //pc.SetLoading(true, "Signing in...");
            
            if (User == null)
            {
                var userLogin = new EuUserLogin(username, password);
                var userBytes =
                    System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(userLogin));

                Debug.Log("json login: " + JsonUtility.ToJson(userLogin));
                WriteLog("json login: " + JsonUtility.ToJson(userLogin));

                var auth = UnityWebRequest.Put(baseUrl + loginHandle, userBytes);
                //var auth = new UnityWebRequest(baseUrl + loginHandle, UnityWebRequest.kHttpVerbPOST);
                auth.method = UnityWebRequest.kHttpVerbPOST;
                //auth.uploadHandler = new UploadHandlerRaw(userBytes);
                auth.SetRequestHeader("Content-Type", "application/json");
                auth.SetRequestHeader("Accept", "application/json");
                //auth.SetRequestHeader("Access-Control-Allow-Origin", baseUrl);
                //auth.uploadHandler.contentType = "application/json";
                //auth.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();
                Debug.Log("Signing in to: " + auth.url);
                WriteLog("Signing in to: " + auth.url);
                var async = auth.SendWebRequest();
                while (!async.isDone)
                {
                    yield return Timing.WaitForOneFrame;
                }
                if (ResourceManager.IsRequestError(auth))
                {
                    Debug.LogError(auth.error);
                    WriteLog(auth.error);
                }
                else
                {
                    Debug.Log(auth.downloadHandler.text);
                    WriteLog(auth.downloadHandler.text);
                    authStatus = JsonUtility.FromJson<EuResponse>(auth.downloadHandler.text);
                }
            }
            
            //pc.SetLoading(false, "");

            if (authStatus == null)
            {
                User = null;
                //pc.SetLoading(true, "Failed to log in.");
                yield return Timing.WaitForSeconds(1f);
                //pc.SetLoading(false, "");
                Debug.Log("Failed to log in : ");
                WriteLog("Failed to log in : ");
            }
            else
            {
                User = authStatus.data.GetUser();
                //pc.SetLoading(true, "Successfully logged in, user id: " + User.id);
                yield return Timing.WaitForSeconds(1f);
                //pc.SetLoading(false, "");
                Debug.Log("Successfully logged in, user id: " + User.id);
                WriteLog("Successfully logged in, user id: " + User.id);
            }
            loggingIn = false;
        }

        public IEnumerator<float> PostGuest(string email, string phone)
        {
            //var trainingData = JsonUtility.ToJson(new EuTrainingScore(score, trainingId, User.id.ToString()));
            var guestData = JsonUtility.ToJson(new EuGuestUser(email, phone));
            /*guestData = guestData.Replace("username\":", scoreUserParameter + "\":");*/
            var post = UnityWebRequest.Put("http://toyota-staging.machinevision.global:2020/" + "guest-users", System.Text.Encoding.UTF8.GetBytes(guestData));
            post.method = UnityWebRequest.kHttpVerbPOST;
            post.SetRequestHeader("Content-Type", "application/json");
            post.SetRequestHeader("Accept", "application/json");
            post.uploadHandler.contentType = "application/json";
            var async = post.SendWebRequest();
            Debug.Log("Try to post guest");
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (ResourceManager.IsRequestError(post))
            {
                Debug.LogError(post.error);
            }
            else
            {
                Debug.Log("New Guest Entry Added. response: " + post.downloadHandler.text);
                guestUser = true;
            }
        }

        public IEnumerator<float> PostScore(int trainingId, int score, string type)
        {
#if UNITY_EDITOR
            if (User == null || string.IsNullOrEmpty(User.token))
                yield return Timing.WaitUntilDone(SignIn(debugUsername, debugPassword));
#endif
            if (User == null)
                yield break;
            if (string.IsNullOrEmpty(User.token))
            {
                Debug.LogWarning("User isn't signed in, failed to post score.");
                WriteLog("User isn't signed in, failed to post score.");
                yield break;
            }
            else
            {
                Debug.Log("Uploading score to:\r\ntraining_id: " + trainingId + "\r\nscore: " + score + "\r\nbearer: " + User.token);
                WriteLog("Uploading score to:\r\ntraining_id: " + trainingId + "\r\nscore: " + score + "\r\nbearer: " + User.token);
            }

            //var trainingData = JsonUtility.ToJson(new EuTrainingScore(score, trainingId, User.id.ToString()));
            var trainingData = JsonUtility.ToJson(new EuTrainingScoreToken(score, trainingId/*, type*/));
            trainingData = trainingData.Replace("username\":", scoreUserParameter + "\":");
            var post = UnityWebRequest.Put(baseUrl + scoreHandle, System.Text.Encoding.UTF8.GetBytes(trainingData));
            Debug.Log("Post : \n" + post.url + "\nHandler : \n" + System.Text.Encoding.UTF8.GetString(post.uploadHandler.data));
            WriteLog("Post : \n" + post.url + "\nHandler : \n" + System.Text.Encoding.UTF8.GetString(post.uploadHandler.data));
            post.method = UnityWebRequest.kHttpVerbPOST;
            post.SetRequestHeader("Content-Type", "application/json");
            post.SetRequestHeader("Accept", "application/json");
            post.SetRequestHeader("Authorization", "Bearer " + User.token);
            post.uploadHandler.contentType = "application/json";
            var async = post.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }
            
            if (ResourceManager.IsRequestError(post))
            {
                Debug.LogError(post.error);
                WriteLog(post.error);
            }
            else
            {
                Debug.Log("Score submitted. response: " + post.downloadHandler.text); 
                WriteLog("Score submitted. response: " + post.downloadHandler.text);
            }
        }

        private string log = "LOG";

        public void WriteLog(string content)
        {
            if (UILog)
            {
                log += ("\n" + content);
                GetComponentInChildren<TextMeshProUGUI>().text = log;
            }
        }

        [Button]
        public void TestGetScore()
        {
#if UNITY_EDITOR
            Timing.RunCoroutine(GetScore(debugTrainingId, debugType));
#endif
        }

        public EuFetchScoreResponse scoresResponse { get; set; }

        public IEnumerator<float> GetScore()
        {
            CoroutineHandle c = Timing.RunCoroutine(GetScore(-1,"SUBSCENARIO"));
            while (c.IsRunning)
                yield return Timing.WaitForSeconds(.1f);
        }

        public List<string> id = new List<string>();
        public byte[] result;
        public IEnumerator<float> GetScore(int trainingId, string type)
        {
            scoresResponse = null;

#if UNITY_EDITOR
            if (User == null)
                yield return Timing.WaitUntilDone(SignIn(debugUsername, debugPassword));
#endif
            if (User == null)
                Debug.Log("Can't find user");
            string id = null;
            if (trainingId > 0)
                id = trainingId.ToString();
            Debug.Log("Score URL: " + baseUrl + "training/user-3d/" + id + "?type=" + type);
            var get = UnityWebRequest.Get(baseUrl + "training/user-3d/" + id + "?type=" + type);

            get.SetRequestHeader("Content-Type", "application/json");
            get.SetRequestHeader("Accept", "application/json");
            if(User != null)
                get.SetRequestHeader("Authorization", "Bearer " + User.token);

            var async = get.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (ResourceManager.IsRequestError(get))
            {
                Debug.LogError(get.error);
            }
            else
            {
                Debug.Log(get.downloadHandler.text);
                scoresResponse = JsonUtility.FromJson<EuFetchScoreResponse>(get.downloadHandler.text);

                if (scoresResponse == null)
                    Debug.Log("jancok");
                else
                    Debug.Log(scoresResponse.data);
            }

            if (scoresResponse == null)
            {
                UserScores = null;
                Debug.LogWarning("No score responses");
                yield return Timing.WaitForSeconds(1);
            }

            else
            {
                UserScores = scoresResponse.data;
                //Debug.Log(string.Join("\n", UserScores));
                yield return Timing.WaitForSeconds(1);
            }
        }
    }
}