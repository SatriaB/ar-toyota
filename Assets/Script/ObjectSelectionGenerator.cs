using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectSelectionGenerator : MonoBehaviour
{
    public string layerName = "Selectable";
    public float basePreviewSize = 1;
    bool generated = false;

    private void Start()
    {
        GenerateSelection();
    }

    private void OnEnable()
    {
        GenerateSelection();
    }

    private void GenerateSelection()
    {
        if (!generated)
        {
            List<Transform> t = GetComponentsInChildren<Transform>(true).Where(x => x.gameObject.layer == LayerMask.NameToLayer(layerName)).ToList();
            foreach (var item in t)
            {
                ObjectSelection os;
                if (item.gameObject.GetComponent<ObjectSelection>())
                    os = item.gameObject.GetComponent<ObjectSelection>();
                else
                {
                    os = item.gameObject.AddComponent<ObjectSelection>();
                    os.layerName = layerName;
                    os.previewSize = basePreviewSize;
                }
            }
            generated = true;
        }
    }
}
