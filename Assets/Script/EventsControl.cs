using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;

using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

public class EventsControl : MonoBehaviour
{
    public static EventsControl Instance;
    [ReadOnly]
    public ObjectSelection selectedObject;
    public GameObject previewCamera;
    public GameObject previewPanel;
    public GameObject infoPrefab;
    public RectTransform summonInfoPos;
    public RectTransform Pagination;
    
    public Image zoomPreview;
    [ReadOnly]
    public int currentIndex, currentPagination;
    public bool onPreview = false;
    [ReadOnly] public bool lerping = false;
    public bool enabledInfo = false;
    public AssemblyState assemblyState = AssemblyState.Assembled;
    [Header("General")]
    public GameObject generalInfoButton;
    public InfoPanel generalInfoPanel;
    public GameObject generalInfoContent;
    [Header("Object Part")]
    public InfoPanel partInfoPanel;

    //public UnityEvent SavedEvents;

    public enum AssemblyState
    {
        Assembled,
        Disassembled
    }

    public void assembleState(string state)
    {
        assemblyState = (AssemblyState)System.Enum.Parse(typeof(AssemblyState), state);
    }

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    private void OnLevelWasLoaded(int level)
    {
        Screen.orientation = ScreenOrientation.Landscape;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Screen.orientation != ScreenOrientation.Landscape)
            Screen.orientation = ScreenOrientation.Landscape;
    }

    public void setOnPreview(bool b)
    {
        onPreview = b;
    }

    public void growIn(GameObject go)
    {
        Timing.RunCoroutine(GrowIn(go));
    }

    IEnumerator<float> GrowIn(GameObject go)
    {
        Debug.Log("zoom : " + go + " | " + go.activeSelf);

        if (!go.activeSelf)
        {
            lerping = true;
            go.transform.localScale = Vector3.zero;
            go.gameObject.SetActive(true);
            go.transform.localScale = Vector3.zero;
            go.transform.DOScale(Vector3.one, .5f);
            yield return Timing.WaitForSeconds(.5f);
            yield return Timing.WaitForOneFrame;
            go.transform.localScale = Vector3.one;
            lerping = false;
        }
    }

    public void shrinkOut(GameObject go)
    {
        Timing.RunCoroutine(ShrinkOut(go));
    }

    public void closePreview(GameObject go)
    {
        Timing.RunCoroutine(unzoom(go));
    }
    IEnumerator<float> unzoom(GameObject go)
    {
        GameObject zoom = go.gameObject;
        if (zoom.transform.childCount > 0)
            zoom = go.transform.GetChild(0).gameObject;
        /*CoroutineHandle c = */Timing.RunCoroutine(ShrinkOut(zoom));
        /*if (c.IsRunning)
            yield return Timing.WaitForOneFrame;
        yield return Timing.WaitForOneFrame;*/
        yield return Timing.WaitForSeconds(.5f);
        go.gameObject.SetActive(false);
        //yield return Timing.WaitForOneFrame;
        if (zoom != go)
        {
            zoom.transform.localScale = Vector3.one;
            zoom.SetActive(true);
        }
    }

    IEnumerator<float> ShrinkOut(GameObject go)
    {
        if (go.activeSelf)
        {
            lerping = true;
            go.transform.localScale = Vector3.one;
            go.transform.DOScale(Vector3.zero, .5f);
            yield return Timing.WaitForSeconds(.5f);
            yield return Timing.WaitForOneFrame;
            go.gameObject.SetActive(false);
            lerping = false;
        }
    }

    public void deselect() {
        if(selectedObject)
            Destroy(selectedObject.transform.GetComponent<ObjectSelection>().duplicate);
        onPreview = false;
        selectedObject = null;
        currentIndex = 0;
        currentPagination = 0;
        enabledInfo = false;
        generalInfoButton.SetActive(true);
    }

    public void setContentValue()
    {
        generalInfoContent.transform.GetChild(generalInfoPanel.GetComponentInChildren<TMP_Dropdown>().value).GetComponentInChildren<InfoPoint>().setContent();
    }
}
