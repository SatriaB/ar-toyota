﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using Tictech.EnterpriseUniversity;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class TransformUtility : MonoBehaviour
{
    [Header("On Load Level")]
    public UnityEvent onLoad;
    [Header("On Start")]
    public UnityEvent onStart;
    public void SingleLoadScene(string name)
    {
        SceneManager.LoadScene(name, LoadSceneMode.Single); 
        /*if (EuRuntimeManager.Instance)
            SceneManager.MoveGameObjectToScene(EuRuntimeManager.Instance.gameObject, SceneManager.GetSceneByName(name));*/
    }
    public void LoadARScene(string name) {
        LoaderUtility.Initialize();
        SceneManager.LoadScene(name, LoadSceneMode.Single);
        /*if (EuRuntimeManager.Instance)
            SceneManager.MoveGameObjectToScene(EuRuntimeManager.Instance.gameObject, SceneManager.GetSceneByName(name));*/
    }

    public void LoadNonARScene(string name)
    {
        SceneManager.LoadScene(name, LoadSceneMode.Single);
        /*if (EuRuntimeManager.Instance)
            SceneManager.MoveGameObjectToScene(EuRuntimeManager.Instance.gameObject, SceneManager.GetSceneByName(name));*/
        LoaderUtility.Deinitialize();
    }

    public Transform authPanel, menuPanel;
    [Header("Sign In")]
    public TextMeshProUGUI usernameMessage;
    public TextMeshProUGUI passwordMessage;
    public TMP_InputField usernameField, passwordField;
    
    public UnityEvent onSignIn = new UnityEvent();

    [Header("Guest Enter")]
    public TextMeshProUGUI emailMessage;
    public TextMeshProUGUI phoneMessage;
    public TMP_InputField emailField, phoneField;

    public UnityEvent onGuestEnter = new UnityEvent();

    private void Start()
    {
        onStart.Invoke();
    }

    private void OnLevelWasLoaded(int level)
    {
        onLoad.Invoke(); 
    }

    public void CheckLogin()
    {
        if (authPanel && menuPanel)
            if (EuRuntimeManager.Instance)
            {
                if (EuRuntimeManager.Instance.User == null && EuRuntimeManager.Instance.guestUser == false)
                    return;
                authPanel.gameObject.SetActive(false);
                menuPanel.gameObject.SetActive(true);
            }
            else
            {
                authPanel.gameObject.SetActive(false);
                menuPanel.gameObject.SetActive(true);
            }
    }

    public void Sign_Out()
    {
        if (EuRuntimeManager.Instance)
        {
            EuRuntimeManager.Instance.SignOutUser();
            EuRuntimeManager.Instance.guestUser = false;
        }
        if(authPanel)
            authPanel.gameObject.SetActive(true);
        if(menuPanel)
            menuPanel.gameObject.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        /*if (EuRuntimeManager.Instance)
            SceneManager.MoveGameObjectToScene(EuRuntimeManager.Instance.gameObject, SceneManager.GetActiveScene());*/
    }

    public void SignIn()
    {
        Timing.RunCoroutine(Sign_In().CancelWith(gameObject).CancelWith(gameObject));
    }

    public void GuestEnter()
    {
        Timing.RunCoroutine(Guest_Enter().CancelWith(gameObject).CancelWith(gameObject));
    }
    public GameObject arSession;
    public void checkArSession()
    {
        if (!GameObject.FindObjectOfType<ARSession>() && arSession) { 
            arSession.SetActive(true);
            DontDestroyOnLoad(arSession);
        }
    }

    public IEnumerator<float> Guest_Enter()
    {
        if (emailMessage)
            emailMessage.text = "";
        if (phoneMessage)
            phoneMessage.text = "";

        bool valid = true;
        if (emailField)
        {
            if (string.IsNullOrEmpty(emailField.text))
            {
                if (emailMessage)
                {
                    emailMessage.text = "* E-mail dibutuhkan";
                    emailMessage.color = Color.red;
                    valid = false;
                }
            }
        }
        else valid = false;

        if (phoneField)
        {
            if (string.IsNullOrEmpty(phoneField.text))
            {
                if (phoneMessage)
                {
                    phoneMessage.text = "* Nomor Ponsel dibutuhkan";
                    phoneMessage.color = Color.red;
                    valid = false;
                }
            }
        }
        else valid = false;

        if (!valid)
            yield break;
        CoroutineHandle g = Timing.RunCoroutine(EuRuntimeManager.Instance.PostGuest(emailField.text, phoneField.text).CancelWith(gameObject));
        while (g.IsRunning)
            yield return Timing.WaitForOneFrame;
        if (!EuRuntimeManager.Instance.guestUser)
            Debug.LogError("Fail To Post Guest (Guest : " + EuRuntimeManager.Instance.guestUser + ")");
        if(authPanel)
            authPanel.gameObject.SetActive(false);
        if(menuPanel) 
            menuPanel.gameObject.SetActive(true);
        onGuestEnter.Invoke();
    }

    IEnumerator<float> Sign_In()
    {
        usernameMessage.text = "";
        passwordMessage.text = "";

        bool valid = true;
        if(usernameField)
        if (string.IsNullOrEmpty(usernameField.text))
        {
            usernameMessage.text = "* E-mail atau ID Number dibutuhkan";
            usernameMessage.color = Color.red;
            valid = false;
        }

        if(passwordField)
        if (string.IsNullOrEmpty(passwordField.text))
        {
            passwordMessage.text = "* Password dibutuhkan";
            passwordMessage.color = Color.red;
            valid = false;
        }

        if (!valid)
            yield break;
        CoroutineHandle h = Timing.RunCoroutine(EuRuntimeManager.Instance.SignIn(usernameField.text, passwordField.text).CancelWith(gameObject));
        while (h.IsRunning)
            yield return Timing.WaitForOneFrame;
        while (EuRuntimeManager.Instance.loggingIn)
            yield return Timing.WaitForOneFrame;
        if (EuRuntimeManager.Instance.User == null)
            yield break;
        if(authPanel)
            authPanel.gameObject.SetActive(false);
        if(menuPanel)
            menuPanel.gameObject.SetActive(true);
        onSignIn.Invoke();
    }
}
