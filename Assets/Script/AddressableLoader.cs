using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tictech.ResourceManager;
using UnityEngine.AddressableAssets;

public class AddressableLoader : MonoBehaviour
{
    public AssetReference prefabRefernce;
    public Transform root;

    // Start is called before the first frame update
    void Start()
    {
        ResourceManager.LoadAddressable(prefabRefernce, delegate ()
        {
            prefabRefernce.InstantiateAsync(root).Completed += (async) =>
            {
                Debug.Log(async.Result.name);
                ResourceManager.UnloadResource(prefabRefernce.AssetGUID, true);
                Destroy(this);
            };
        });
    }
}
