using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RotationControl : MonoBehaviour
{
    [SerializeField, ReadOnly]
    Quaternion start = new Quaternion();
    [SerializeField, ReadOnly]
    Vector3 begin, rotation;
    Vector3 screen = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        transform.parent.localRotation = Quaternion.identity;
        transform.parent.parent.localRotation = Quaternion.identity;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            begin = transform.parent.localEulerAngles;
            rotation = transform.parent.parent.localEulerAngles;
            screen = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            transform.parent.localRotation = Quaternion.Euler(begin.x, 
                begin.y + (screen.x - Input.mousePosition.x) * .2f, begin.z);
            transform.parent.parent.localRotation = Quaternion.Euler(
                rotation.x - (screen.y - Input.mousePosition.y) * .2f,
                rotation.y, rotation.z);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            transform.parent.parent.DOLocalRotateQuaternion(Quaternion.identity, .5f);
        }
        else
        {
            transform.parent.localRotation = Quaternion.Euler(transform.parent.localEulerAngles.x,
                transform.parent.localEulerAngles.y + (Time.deltaTime * 20), transform.parent.localEulerAngles.z);
        }
    }
}
