using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class StageManager : MonoBehaviour
{
    [System.Serializable]
    public class Stage {
        public Button sceneLoadButton;
        public GameObject interactableActive, uninteractableActive;
    }
    public List<Stage> stages;
    // Start is called before the first frame update
    void Start()
    {
        checkInteractionState();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void checkInteractionState()
    {
        if (stages.Count > 0)
            foreach (var item in stages)
            {
                if (item.sceneLoadButton.interactable)
                {
                    item.interactableActive.gameObject.SetActive(true);
                    item.uninteractableActive.gameObject.SetActive(false);
                }
                else {
                    item.uninteractableActive.gameObject.SetActive(true);
                    item.interactableActive.gameObject.SetActive(false);
                }
            }
    }
}
