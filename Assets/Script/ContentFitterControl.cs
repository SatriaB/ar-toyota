using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContentFitterControl : MonoBehaviour
{
    public bool forceSize = true;
    public float preferredHeight = 210;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (GetComponent<LayoutElement>() && forceSize)
            if (GetComponent<RectTransform>().rect.height > preferredHeight + .001f)
            {
                GetComponent<LayoutElement>().preferredHeight = preferredHeight;
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetComponent<RectTransform>());
            }
            else;
        else if (GetComponent<RectTransform>().rect.height > preferredHeight + .001f)
            LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetComponent<RectTransform>());
    }
}
