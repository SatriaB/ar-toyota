using MEC;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoPoint : MonoBehaviour
{
    [System.Serializable]
    public class Fungsi
    {
        [TextArea] public string detail;
        [TextArea] public List<string> details;
    }

    [System.Serializable]
    public class Dentifikasi
    {
        public Sprite image;
        [TextArea] public string detail;
    }
    [System.Serializable]
    public class AbnormalityEffect
    {
        [TextArea] public string detail;
        [TextArea] public List<string> details;
    }

    [System.Serializable]
    public class Information {
        public string title;
        public Fungsi fungsi;
        public Dentifikasi dentifikasi;
        public AbnormalityEffect abnormalityEffect;
    }
    public Subject subject;
    public enum Subject
    {
        fungsi, dentifikasi, abnoramlity, none
    }

    public enum InfoType
    {
        Part, General
    }

    public InfoType infoType;
    public string title;
    public List<Information> informations;
    public GameObject infoPrefab;
    public RectTransform summonPosition;
    public bool activeOnStart = false;
    [SerializeField, ReadOnly]
    GameObject infoPanel;
    GameObject fungsi, dentifikasi, abnormalEfek;
    List<Button> pageButton = new List<Button>();
    Button bFungsi, bDentifikasi, bAbnormality, bPagePrev, bPageNext, bPageChange;

    public InfoPoint()
    {
        if (EventsControl.Instance)
        {
            if (!infoPrefab && EventsControl.Instance.infoPrefab)
                infoPrefab = EventsControl.Instance.infoPrefab;
            else Debug.LogError("No Info Base Prefab");

            if (!summonPosition && EventsControl.Instance.summonInfoPos)
                summonPosition = EventsControl.Instance.summonInfoPos;
            else Debug.LogError("No Info Pos");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //Timing.RunCoroutine(LateStart());
        //Timing.RunCoroutine(_setContent());
    }

    public IEnumerator<float> _setContent()
    {
        yield return Timing.WaitForSeconds(.1f);
        setContent();
    }

    public void setContent()
    {
        if (EventsControl.Instance)
        {
            EventsControl e = EventsControl.Instance;
            InfoPanel _infoPanel = null;
            if (infoType == InfoType.General && e.generalInfoPanel)
                _infoPanel = e.generalInfoPanel;
            else if (infoType == InfoType.Part && e.partInfoPanel)
            {
                _infoPanel = e.partInfoPanel;
                _infoPanel.title.text = title;
            }
            else return;
            e.onPreview = true;
            if (_infoPanel.objectList.Count > 0)
                foreach (var item in _infoPanel.objectList)
                    Destroy(item.gameObject);
            if (_infoPanel.subObjectList.Count > 0)
                foreach (var item3 in _infoPanel.subObjectList)
                    Destroy(item3.gameObject);
            _infoPanel.objectList.Clear();
            if (informations.Count > 0)
                foreach (var item in informations)
                {
                    GameObject Instance = Instantiate(_infoPanel.listPrefab, _infoPanel.listPanel);
                    Instance.name = item.title;
                    Instance.GetComponentInChildren<TextMeshProUGUI>().text = item.title;
                    Instance.SetActive(true);
                    Instance.GetComponentInChildren<Button>().onClick.AddListener(() =>
                    {
                        if (_infoPanel.subObjectList.Count > 0)
                            foreach (var item3 in _infoPanel.subObjectList)
                                Destroy(item3.gameObject);
                        _infoPanel.subObjectList.Clear();
                        if (item.fungsi.details.Count > 0)
                            foreach (var item2 in item.fungsi.details)
                            {
                                _infoPanel.getInfo("Fungsi").basePanel.gameObject.SetActive(true);
                                fillText(_infoPanel, "Fungsi", item2);
                            }
                        else _infoPanel.getInfo("Fungsi").basePanel.gameObject.SetActive(false);
                        if (item.abnormalityEffect.details.Count > 0)
                            foreach (var item2 in item.abnormalityEffect.details)
                            {
                                _infoPanel.getInfo("Abnormality").basePanel.gameObject.SetActive(true);
                                fillText(_infoPanel, "Abnormality", item2);
                            }
                        else _infoPanel.getInfo("Abnormality").basePanel.gameObject.SetActive(false);
                        if (item.dentifikasi.image)
                        {
                            _infoPanel.getInfo("Dentifikasi").basePanel.gameObject.SetActive(true);
                            fillImage(_infoPanel, "Dentifikasi", item.dentifikasi.image);
                        }
                        else _infoPanel.getInfo("Dentifikasi").basePanel.gameObject.SetActive(false);
                        if (_infoPanel.objectList.Count > 0)
                            foreach (var item4 in _infoPanel.objectList)
                            {
                                Button b = item4.GetComponentInChildren<Button>(true);
                                ColorBlock cb = b.colors;
                                if (item4 == Instance)
                                    cb.normalColor = _infoPanel.selectedColor;
                                else cb.normalColor = _infoPanel.baseColor;
                                b.colors = cb;
                                //Debug.Log(item4.GetComponentInChildren<Button>(true).colors.normalColor);
                            }
                    });
                    _infoPanel.objectList.Add(Instance);
                }
            if (_infoPanel.objectList.Count > 0) _infoPanel.objectList[0].GetComponentInChildren<Button>().onClick.Invoke();
            LayoutRebuilder.ForceRebuildLayoutImmediate(_infoPanel.GetComponent<RectTransform>());

        }
        else Debug.LogError("No Events Control found");
    }

    public void fillText(InfoPanel _infoPanel, string tag, string item2)
    {
        GameObject ins = Instantiate(_infoPanel.getInfo(tag).template, _infoPanel.getInfo(tag).infoArea);
        ins.SetActive(true);
        ins.GetComponentInChildren<TextMeshProUGUI>().text = item2;
        _infoPanel.subObjectList.Add(ins);
    }

    public void fillImage(InfoPanel _infoPanel, string tag, Sprite item2)
    {
        if(item2)
        _infoPanel.getInfo(tag).infoArea.GetComponentInChildren<Image>().sprite = item2;
    }
}
