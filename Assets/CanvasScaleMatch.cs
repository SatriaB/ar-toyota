using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScaleMatch : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in GetComponentsInChildren<CanvasScaler>())
        {
            if ((float)Screen.width / (float)Screen.height < (16f / 9f) - .01f) item.matchWidthOrHeight = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
