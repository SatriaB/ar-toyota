using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicMeshUtility : MonoBehaviour
{
    public Transform target;

    public bool mergeSubMesh;

    public List<Material> materials = new List<Material>();
    public List<Mesh> meshes = new List<Mesh>();

    /*public void CombineChildMeshes()
    {
        int subMeshIndex = 0;
        Material mat;
        MeshFilter targetMeshFilter;
        MeshFilter[] meshFilters = target.GetComponentsInChildren<MeshFilter>();
        CombineInstance[] instance = new CombineInstance[meshFilters.Length];
        List<Material> materials = new List<Material>();

        for(int i=0; i<meshFilters.Length; i++)
        {
            instance[i].mesh = meshFilters[i].sharedMesh;
            instance[i].transform = meshFilters[i].transform.localToWorldMatrix;

            mat = meshFilters[i].GetComponent<MeshRenderer>()?.sharedMaterial;

            if (i==0)
            {
                materials.Add(mat);
            }
            else
            {
                if(!materials.Contains(mat))
                {
                    materials.Add(mat);
                }
            }
            subMeshIndex = materials.IndexOf(mat);

            instance[i].subMeshIndex = subMeshIndex;

            Debug.Log("Object "+meshFilters[i].name+" has material "+mat+". Setting subMesh index to: "+subMeshIndex);
        }

        targetMeshFilter = combinedMeshContainer.GetComponent<MeshFilter>();

        if(!targetMeshFilter)
        {
            combinedMeshContainer.gameObject.AddComponent<MeshFilter>();
        }
        else
        {
            targetMeshFilter.mesh = new Mesh();
            targetMeshFilter.mesh.CombineMeshes(instance, mergeSubMesh);
        }

        
    }
*/
    public void MergeMesh()
    {
        GetSubMeshes();

        Mesh tmp;
        List<CombineInstance> instances = new List<CombineInstance>();
        CombineInstance combineInstance = new CombineInstance();

        meshes.Clear();

        for (int i=0; i<materials.Count; i++)
        {
            instances.Clear();

            foreach(var mf in target.GetComponentsInChildren<MeshFilter>())
            {
                if(mf.GetComponent<MeshRenderer>().sharedMaterial == materials[i] && mf.transform != target)
                {
                    combineInstance.mesh = mf.sharedMesh;
                    combineInstance.transform = mf.transform.localToWorldMatrix;
                }
                instances.Add(combineInstance);
            }

            tmp = new Mesh();
            tmp.CombineMeshes(instances.ToArray());
            tmp.name = materials[i].name;
            meshes.Add(tmp);
        }

        CombineInstance[] finalInstance = new CombineInstance[meshes.Count];

        for(int i=0; i<meshes.Count; i++)
        {
            finalInstance[i].mesh = meshes[i];
            finalInstance[i].transform = Matrix4x4.identity;
        }

        /*combinedMesh = new Mesh();
        combinedMesh.CombineMeshes(finalInstance, false);*/

        MeshFilter meshFilter = target.GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            meshFilter = target.gameObject.AddComponent<MeshFilter>();
        }

        meshFilter.sharedMesh = new Mesh();
        meshFilter.sharedMesh.name = target.name;
        meshFilter.sharedMesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        meshFilter.sharedMesh.CombineMeshes(finalInstance, false);

        SetMeshRenderer();

        Debug.Log(target.childCount);
        int k = target.childCount;

        for (int i = 0; i < k; i++)
        {
            DestroyImmediate(target.GetChild(0).gameObject);
        }
    }

    public void GetSubMeshes()
    {
        Material mat;
        MeshRenderer[] meshRenderers = target.GetComponentsInChildren<MeshRenderer>();

        materials.Clear();

        for(int i=0; i<meshRenderers.Length; i++)
        {
            mat = meshRenderers[i].sharedMaterial;
            if (i==0)
            {
                materials.Add(mat);
            }
            else
            {
                if(!materials.Contains(mat))
                {
                    materials.Add(mat);
                }
            }
        }
    }

    public void SetMeshRenderer()
    {
        MeshRenderer mr = target.gameObject.AddComponent<MeshRenderer>();

        mr.materials = new Material[materials.Count];

        mr.materials = materials.ToArray();
    }

    /*public void WeldVertices(Mesh aMesh)
    {
        for (int j = 0; j < materials.Count; j++)
        {
            int startVert, endVert;
            startVert = aMesh.GetSubMesh(j).firstVertex;
            endVert = startVert + aMesh.GetSubMesh(j).indexCount - 1;

            var verts = aMesh.vertices.Skip(startVert).Take(endVert).ToArray();
            var normals = aMesh.normals.Skip(startVert).Take(endVert).ToArray();
            var uvs = aMesh.uv.Skip(startVert).Take(endVert).ToArray();

            Dictionary<Vector3, int> duplicateHashTable = new Dictionary<Vector3, int>();
            List<int> newVerts = new List<int>();
            int[] map = new int[verts.Length];

            //create mapping and find duplicates, dictionaries are like hashtables, mean fast
            for (int i = 0; i < verts.Length; i++)
            {
                if (!duplicateHashTable.ContainsKey(verts[i]))
                {
                    duplicateHashTable.Add(verts[i], newVerts.Count);
                    map[i] = newVerts.Count;
                    newVerts.Add(i);
                }
                else
                {
                    map[i] = duplicateHashTable[verts[i]];
                }
            }

            // create new vertices
            var verts2 = new Vector3[newVerts.Count];
            var normals2 = new Vector3[newVerts.Count];
            var uvs2 = new Vector2[newVerts.Count];
            for (int i = 0; i < newVerts.Count; i++)
            {
                int a = newVerts[i];
                verts2[i] = verts[a];
                normals2[i] = normals[a];
                uvs2[i] = uvs[a];
            }
            // map the triangle to the new vertices
            var tris = aMesh.triangles;
            for (int i = 0; i < tris.Length; i++)
            {
                tris[i] = map[tris[i]];
            }

            weldedMesh = new Mesh();

            weldedMesh.triangles = tris;
            weldedMesh.vertices = verts2;
            weldedMesh.normals = normals2;
            weldedMesh.uv = uvs2;

            weldedMesh.RecalculateBounds();
            weldedMesh.RecalculateNormals();
        }
    }*/

    /*public void ShowSubMeshesProperties()
    {
        MeshFilter[] meshFilters = target.GetComponentsInChildren<MeshFilter>();

        for(int i=0; i<meshFilters.Length; i++)
        {
            Debug.Log(meshFilters[i].sharedMesh.GetSubMesh(i));
        }
    }*/
}
