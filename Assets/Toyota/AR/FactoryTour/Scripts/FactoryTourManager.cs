
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARFoundation.Samples;
using UnityEngine.UI;

public class FactoryTourManager : MonoBehaviour
{
    public static FactoryTourManager instance;

    public PrefabImagePairManager prefabImagePairManager;
    public VideoPlayerManager videoPlayerManager;

    private static ARTrackedImageManager ARTrackedImageManager;

    [Header("UI")]
    public GameObject beforeScan;
    public GameObject afterScan;
    public GameObject playVideoButtonObject;

    public RectTransform videoPlayerView;

    public Slider zoomSlider;

    public TMP_Text sliderText;

    public GameObject instantiatedObject;

    void Start()
    {
        instance = this;

        ARTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();


        prefabImagePairManager.onTrackedImage += AssignTrackedObject;
        ShowScannedMarkerView(false);
    }

    private void Update()
    {
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.A))
        {
            ShowScannedMarkerView(true);
        }
#endif
    }

    public void AssignTrackedObject(GameObject val)
    {
        instantiatedObject = val;
        zoomSlider.onValueChanged.AddListener((value) =>
        {
            instantiatedObject.transform.localScale = Vector3.one * .025f * value;
            sliderText.text = zoomSlider.value.ToString();
        });
        zoomSlider.value = 1f;
        zoomSlider.gameObject.SetActive(true);
    }

    public void HideSlider()
    {
        zoomSlider.onValueChanged.RemoveAllListeners();
        instantiatedObject = null;
    }

    public void ShowScannedMarkerView(bool value)
    {
        beforeScan.SetActive(!value);
        afterScan.SetActive(value);
    }

    public void ExitVideoPlayerView()
    {
        videoPlayerView.gameObject.SetActive(false);
        playVideoButtonObject.SetActive(true);

        videoPlayerManager.videoPlayer.Stop();
        videoPlayerManager.videoPlayer.targetTexture.Release();
        videoPlayerManager.videoPlayerProgressBar.value = 0;
        videoPlayerManager.fullScreenVideoPlayerProgressBar.value = 0;
        videoPlayerManager.videoPlayerProgressBar.interactable = false;
        videoPlayerManager.fullScreenVideoPlayerProgressBar.interactable = false;
    }
}
