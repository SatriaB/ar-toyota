using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FactoryTourManager))]
public class FactoryTourManagerEditor : Editor
{
    public FactoryTourManager factoryTourManager;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        /*if(GUILayout.Button("Show target's vertex count"))
        {
            factoryTourManager.CountMeshVertices();
        }
        if (GUILayout.Button("Show total mesh renderer"))
        {
            factoryTourManager.CountMeshRenderer();
        }*/
    }

    void OnEnable()
    {
        factoryTourManager = (FactoryTourManager)target;
    }
}
