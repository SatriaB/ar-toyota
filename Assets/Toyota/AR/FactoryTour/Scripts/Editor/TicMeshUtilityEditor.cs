using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TicMeshUtility))]
public class TicMeshUtilityEditor : Editor
{
    TicMeshUtility ticMeshUtility;

    private void OnEnable()
    {
        ticMeshUtility = (TicMeshUtility)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        /*if(GUILayout.Button("Combine Child Meshes"))
        {
            ticMeshUtility.CombineChildMeshes();
        }*/

        if (GUILayout.Button("Merge Meshes"))
        {
            ticMeshUtility.MergeMesh();
        }

        /*if (GUILayout.Button("Get Sub Meshes"))
        {
            ticMeshUtility.GetSubMeshes();
        }*/

        if (GUILayout.Button("Count Objects"))
        {
            Debug.Log("A. " + ticMeshUtility.target.childCount);
            Debug.Log("B. " + ticMeshUtility.target.GetComponentsInChildren<Transform>().Length);
        }
    }
}
