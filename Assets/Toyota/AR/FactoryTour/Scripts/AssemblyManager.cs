﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AssemblyManager : MonoBehaviour
{
    Animation anim;

    private void OnEnable()
    {
        anim = GetComponent<Animation>();
        StartAnim();
    }

    void StartAnim()
    {
        anim.Play();
        Invoke("StartAnim", 33f);
    }
}
