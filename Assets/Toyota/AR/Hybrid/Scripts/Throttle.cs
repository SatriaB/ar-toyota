using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

namespace Toyota.AR.Hybrid.Scripts
{
    [RequireComponent(typeof(FXController))]
    public class Throttle : MonoBehaviour
    {
        [SerializeField] [ReadOnly] private float throttleFill = 0;
        [SerializeField] private float maxThrottle = 1f;
        [SerializeField] private float lowThrottleThreshold = .5f;
        [SerializeField] private float acceleration = .1f;
        [SerializeField] private float deceleration = .2f;

        [SerializeField] private Animator engineAnimator;
        
        [SerializeField] private ParticleSystem electricParticleSystem;
        [SerializeField] private ParticleSystem engineParticleSystem;
        
        [SerializeField] private FXController fxController;
        
        [SerializeField] private bool _isThrottled = false;
        [SerializeField] private bool _onBreak = false;
        
        [SerializeField] private float minRotationValue;
        [SerializeField] private float maxRotationValue;
        
        [SerializeField] private Transform speedometerTransform;
        
        private static readonly int Speed = Animator.StringToHash("Speed");

        public bool IsThrottled => _isThrottled;

        public float CurrentThrottleFill => throttleFill;
        public float LowThrottleThreshold => lowThrottleThreshold;
        
        //private set => throttleFill = value;
        // Start is called before the first frame update
        void Start()
        {
            electricParticleSystem.Clear();
            electricParticleSystem.Stop();
            engineParticleSystem.Clear();
            engineParticleSystem.Stop();

            if (!fxController)
                fxController = GetComponent<FXController>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_isThrottled && !_onBreak)
            {
                //electricParticleSystem.Play();

                if (!fxController.ElectricFX.IsActive || fxController.ElectricParticle.isStopped)
                {
                    fxController.ActivateFXElectric(true);
                }

                if (!fxController.ElectricPathFlowToMachine)
                {
                    fxController.ElectricPathToMachine(true);
                    fxController.SwitchElectricMeshFlow(true);
                }
                
                throttleFill += acceleration * Time.deltaTime;

                if (throttleFill >= lowThrottleThreshold)
                {
                    //engineParticleSystem.Play();
                    if(!fxController.EngineFX.IsActive || fxController.EngineParticle.isStopped)
                        fxController.ActivateFXEngine(true);
                }

                if (throttleFill > maxThrottle)
                {
                    throttleFill = maxThrottle;
                }
            }

            else
            {
                if (fxController.ElectricPathFlowToMachine && throttleFill > 0)
                {
                    fxController.ElectricPathToMachine(false);
                    fxController.SwitchElectricMeshFlow(false);
                }
                
                throttleFill -= (_onBreak ? deceleration : 0.08f) * Time.deltaTime;

                if (throttleFill < lowThrottleThreshold)
                {
                    if (fxController.EngineFX.IsActive || fxController.EngineParticle.isPlaying)
                    {
                        fxController.ActivateFXEngine(false);
                    }
                }

                if (throttleFill <= 0)
                {
                    throttleFill = 0;

                    /*if (electricParticleSystem.isPlaying)
                    {
                        electricParticleSystem.Clear();
                        electricParticleSystem.Stop();
                    }

                    if (engineParticleSystem.isPlaying)
                    {
                        engineParticleSystem.Clear();
                        engineParticleSystem.Stop();
                    }*/
                    
                    if(fxController.ElectricFX.IsActive || fxController.ElectricParticle.isPlaying)
                        fxController.ActivateFXElectric(false);
                    
                    fxController.SwitchElectricMeshFlow(true);
                }
            }

            engineAnimator.SetFloat(Speed, throttleFill);
            speedometerTransform.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Lerp(minRotationValue, maxRotationValue, Mathf.InverseLerp(0, maxThrottle, throttleFill)));

            /*lowThrottleMaterial.color = Color.Lerp(Color.white, lowThrottleColor, throttleFill);
            highThrottleMaterial.color = Color.Lerp(Color.white, highThrottleColor, throttleFill < lowThrottleThreshold ? 0 : Mathf.Lerp(0, 1,
                (throttleFill - lowThrottleThreshold) * 2));*/
        }

        public void SetThrottle(bool newState)
        {
            _isThrottled = newState;
        }

        public void SetBrake(bool newState)
        {
            _onBreak = newState;
        }
    }
}