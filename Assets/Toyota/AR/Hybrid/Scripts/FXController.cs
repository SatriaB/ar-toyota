using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;


[Serializable]
public class TransparentBodyMaterial
{
    public MeshRenderer mesh;
    public Material transparentMaterial;
    public Material opaqueMaterial;

    public TransparentBodyMaterial(MeshRenderer mesh, Material transparentMaterial, Material opaqueMaterial)
    {
        this.mesh = mesh;
        this.transparentMaterial = transparentMaterial;
        this.opaqueMaterial = opaqueMaterial;
    }
}

public class FXController : MonoBehaviour
{
    public PSMeshRendererUpdater ElectricFX => electricEffectUpdater;
    public PSMeshRendererUpdater EngineFX => engineEffectUpdater;
    public ParticleSystem ElectricParticle => _currentlyPlayingParticleSystem[0];
    public ParticleSystem EngineParticle => engineFXParticle[0];
    public bool ElectricPathFlowToMachine => electricFXPathFollower.speed < 0;
    
    [SerializeField] private PSMeshRendererUpdater electricEffectUpdater;
    [SerializeField] private PSMeshRendererUpdater electricEffectFlowbackUpdater;
    [SerializeField] private PSMeshRendererUpdater engineEffectUpdater;
    [FormerlySerializedAs("electricFXParticle")] [SerializeField] private ParticleSystem[] electricFlowForwardFXParticle;
    [SerializeField] private ParticleSystem[] electricFlowBackFXParticle;
    [SerializeField] private ParticleSystem[] engineFXParticle;
    [SerializeField] private PathFollowerMainController electricFXPathFollower;
    
    [SerializeField] private MeshRenderer[] renderers;
    [SerializeField] private Material baseOpaqueMaterial;
    [SerializeField] private Material baseTransparentMaterial;

    [SerializeField] private GameObject arrowForwardMesh, arrowBackMesh;

    [SerializeField] private float transparencyFactor = 0.2f;
    
    [SerializeField]
    private List<TransparentBodyMaterial> bodyMaterials = new List<TransparentBodyMaterial>();

    private bool _electricFXAccelerate = true;
    private bool _electricFlowSwitching = false;
    private ParticleSystem[] _currentlyPlayingParticleSystem;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        _currentlyPlayingParticleSystem = electricFlowForwardFXParticle;
        
        Setup();
        yield return new WaitForEndOfFrame();
        ActivateFXElectric(true);
        ActivateFXEngine(true);
        
        yield return new WaitForSeconds(1);
        
        ActivateFXElectric(false);
        ActivateFXEngine(false);
        
        //SwitchElectricFlow(true);
        
        //electricEffectUpdater.UpdateMeshEffect();
        yield break;
    }

    [Button]
    public void Setup()
    {
        bodyMaterials.Clear();
        
        foreach (var bodyRenderer in renderers)
        {
            Material opaque = new Material(baseOpaqueMaterial);
            Material transparent = new Material(baseTransparentMaterial);
            Material rendererMaterial = bodyRenderer.sharedMaterial;

            opaque.color = rendererMaterial.color;
            transparent.color = new Color(rendererMaterial.color.r, rendererMaterial.color.g, rendererMaterial.color.b, transparencyFactor);
            
            bodyMaterials.Add(new TransparentBodyMaterial(bodyRenderer, transparent, opaque));
        }
    }

    public void DropdownBodyTransparency(int transparency)
    {
        switch (transparency)
        {
            case 0:
                renderers[0].transform.parent.gameObject.SetActive(true);
                ToggleBodyTransparency(false);
                break;
            case 1:
                renderers[0].transform.parent.gameObject.SetActive(true);
                ToggleBodyTransparency(true);
                break;
            case 2:
                renderers[0].transform.parent.gameObject.SetActive(false);
                break;
        }
    }
    
    public void ToggleBodyTransparency(bool transparency)
    {
        foreach (var transparentBodyMaterial in bodyMaterials)
        {
            transparentBodyMaterial.mesh.material = transparency
                ? transparentBodyMaterial.transparentMaterial
                : transparentBodyMaterial.opaqueMaterial;
        }
    }

    public void ActivateFXElectric(bool value)
    {

        if (_currentlyPlayingParticleSystem == null)
            return;
        arrowForwardMesh.SetActive(value);
        arrowBackMesh.SetActive(value);
        foreach (var particle in _currentlyPlayingParticleSystem)
        {
            
            if (value)
            {
                particle.Play();
            }

            else
            {
                particle.Clear();
                particle.Stop();
            }
        }
    }

    void SwitchElectricFlow(bool forward)
    {
        bool isCurrentlyPlaying = _currentlyPlayingParticleSystem[0].isPlaying;
        
        if (forward)
        {
            SwitchElectricMeshFlow(true);
            
            ActivateFXElectric(false);
            _currentlyPlayingParticleSystem = electricFlowForwardFXParticle;
            
            if (isCurrentlyPlaying)
                ActivateFXElectric(true);
        }

        else
        {
            SwitchElectricMeshFlow(false);
            
            ActivateFXElectric(false);
            
            _currentlyPlayingParticleSystem = electricFlowBackFXParticle;
            if(isCurrentlyPlaying)
                ActivateFXElectric(true);
        
        }
    }
    
    public void SwitchElectricMeshFlow(bool forward)
    {
        electricEffectUpdater.MeshObject.SetActive(forward);
        electricEffectFlowbackUpdater.MeshObject.SetActive(!forward);
    }

    public void ActivateFXEngine(bool value)
    {
        if(engineEffectUpdater)
            engineEffectUpdater.IsActive = value;
        
        foreach (var particle in engineFXParticle)
        {
            if (value)
            {
                particle.Play();
            }

            else
            {
                particle.Stop();
            }
        }
    }

    public void ElectricPathToMachine(bool value)
    {
        if (value == _electricFXAccelerate)
            return;
        SwitchElectricFlow(value);
        _electricFXAccelerate = value;
        electricFXPathFollower.speed = value ? -1 : 1;
    }

    private void OnDestroy()
    {
        ActivateFXElectric(false);
        ActivateFXEngine(false);
    }
}
