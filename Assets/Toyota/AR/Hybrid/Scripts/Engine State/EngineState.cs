using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EngineState
{

    public void OnStart()
    {
        
    }

    public void OnChange()
    {

    }
}