using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class POI
{
    public Transform target;
    public GameObject poiPrefab;
    public string description;
}

public class HybridPOI : MonoBehaviour
{
    [SerializeField] private GameObject poiPrefab;

    [SerializeField] private GameObject poiCanvas;

    [SerializeField] private List<POI> pois = new List<POI>();

    private Camera _mainCam;
    private UIScaleCanvasExtend _poiCanvasScaler;
    
    // Start is called before the first frame update
    void Start()
    {
        _mainCam = Camera.main;
        _poiCanvasScaler = poiCanvas.GetComponent<UIScaleCanvasExtend>();
        
        for (var i = 0; i < pois.Count; i++)
        {
            GameObject newPOI = Instantiate(poiPrefab, poiCanvas.transform);
            pois[i].poiPrefab = newPOI;
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var poi in pois)
        {
            poi.poiPrefab.transform.position = _mainCam.WorldToScreenPoint(poi.target.position);
        }
    }

    public void ShowPOI(int transparencyValue)
    {
        Debug.Log(transparencyValue);
        switch (transparencyValue )
        {
            case 0:
                _poiCanvasScaler.SetShowAnimated(false);
                break;
            case 1:
                _poiCanvasScaler.SetShowAnimated(true);
                break;
            case 2:
                _poiCanvasScaler.SetShowAnimated(true);
                break;
        }
    }
}
