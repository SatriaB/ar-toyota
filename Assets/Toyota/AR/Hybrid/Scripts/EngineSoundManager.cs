using System;
using System.Collections;
using System.Collections.Generic;
using Hellmade.Sound;
using Toyota.AR.Hybrid.Scripts;
using UnityEngine;

public class EngineSoundManager : MonoBehaviour
{
    [SerializeField] private float maxVolume = 1;
    
    [SerializeField] private AudioClip electricClip;
    [SerializeField] private AudioClip engineClip;
    [SerializeField] private AudioClip idleClip;
    
    [SerializeField] private Throttle throttle;

    private Audio _electricAudio;
    private Audio _engineAudio;
    private Audio _idleAudio;

    private int _electricID;
    private int _engineID;
    private int _idleID;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!throttle)
            throttle = GetComponent<Throttle>();

        if (_electricAudio == null)
        {
            _electricID = EazySoundManager.PlaySound(electricClip, 0, true, gameObject.transform);
            _electricAudio = EazySoundManager.GetAudio(_electricID);
        }

        if (_engineAudio == null)
        {
            _engineID = EazySoundManager.PlaySound(engineClip, 0, true, gameObject.transform);
            _engineAudio = EazySoundManager.GetAudio(_engineID);
        }

        if (_idleAudio == null)
        {
            _idleID = EazySoundManager.PlaySound(idleClip, 0.5f, true, gameObject.transform);
            _idleAudio = EazySoundManager.GetAudio(_idleID);
        }

        _electricAudio.Pause();
        _engineAudio.Pause();
    }

    // Update is called once per frame
    void Update()
    {

        if (throttle.CurrentThrottleFill <= 0)
        {
            _idleAudio.SetVolume(0.2f);
            
            if(_engineAudio.Volume > 0)
                _engineAudio.SetVolume(0f);
        }

        else
        {
            if(_engineAudio.Paused)
                _engineAudio.UnPause();
                
            _engineAudio.SetVolume(Mathf.Lerp(0, maxVolume, throttle.CurrentThrottleFill) + 0.1f);
            
            if(_idleAudio.Volume > 0)
                _idleAudio.SetVolume(0);
        }
        
        /*
         
         float volumeLerp = 0f;
         
         switch (
            throttle.CurrentThrottleFill > throttle.LowThrottleThreshold ? "Engine" : 
            throttle.CurrentThrottleFill > 0.1f ? "Electric" : "Idle")
        {
            case "Engine":
                volumeLerp = Mathf.InverseLerp(throttle.LowThrottleThreshold, 1, throttle.CurrentThrottleFill);
                
                if(_electricAudio.Volume > 0)
                    _electricAudio.SetVolume(0, 0.5f);
                else if(_electricAudio.IsPlaying)
                    _electricAudio.Pause();
                
                if(_idleAudio.IsPlaying)
                    _idleAudio.Pause();
                
                if(_engineAudio.Paused)
                    _engineAudio.UnPause();
                _engineAudio.SetVolume(volumeLerp);
                break;
            
            case "Electric":
                volumeLerp = Mathf.InverseLerp(0.1f, throttle.LowThrottleThreshold, throttle.CurrentThrottleFill);
                
                if(_engineAudio.Volume > 0)
                    _engineAudio.SetVolume(0, 0.5f);
                else if(_engineAudio.IsPlaying)
                    _engineAudio.Pause();
                
                if(_idleAudio.IsPlaying)
                    _idleAudio.Pause();
                
                if(_electricAudio.Paused)
                    _electricAudio.UnPause();
                _electricAudio.SetVolume(throttle.CurrentThrottleFill, 0.1f);
                break;
            
            case "Idle":
                _electricAudio.Pause();
                _engineAudio.Pause();
                
                _idleAudio.UnPause();
                break;
        }*/
    }

    private void OnDisable()
    {
        _idleAudio.Pause();
        _electricAudio.Pause();
        _engineAudio.Pause();
    }
}
