using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;

public class PathFollowerMainController : MonoBehaviour
{
    public PathCreator pathCreator;
    public EndOfPathInstruction endOfPathInstruction;
    public float speed = 5;
    public float distanceTravelled;
    public float offset = 0;

    public bool partOfFollowers = false;
    public bool move = false;

    public bool CanMove
    {
        get => move;
        set => move = value;
    }

    private List<GameObject> _followers = new List<GameObject>();

    void Start() {
        if (pathCreator != null)
        {
            // Subscribed to the pathUpdated event so that we're notified if the path changes during the game
            pathCreator.pathUpdated += OnPathChanged;
            transform.position = pathCreator.path.GetPointAtDistance(offset, endOfPathInstruction);
            transform.rotation = pathCreator.path.GetRotationAtDistance(offset, endOfPathInstruction);

            if (transform.childCount > 0)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    _followers.Add(transform.GetChild(i).gameObject);
                }
            }
        }
    }

    void Update()
    {
        if (pathCreator != null && move)
        {
            if(!partOfFollowers)
                distanceTravelled += speed * Time.deltaTime;

            for (int i = 0; i < _followers.Count; i++)
            {
                _followers[i].transform.position = pathCreator.path.GetPointAtTime((speed * Time.time) + (offset * i), endOfPathInstruction);
                _followers[i].transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled + (offset * i), endOfPathInstruction);
            }
            
            
        }
    }

    // If the path changes during the game, update the distance travelled so that the follower's position on the new path
    // is as close as possible to its position on the old path
    void OnPathChanged() {
        distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
    }
}
