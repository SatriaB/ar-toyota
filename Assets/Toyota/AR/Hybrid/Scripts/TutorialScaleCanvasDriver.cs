﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScaleCanvasDriver : MonoBehaviour
{
    [SerializeField] private UIScaleCanvasExtend[] firstScaleCanvases;
    
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        
        foreach (var scaleCanvas in firstScaleCanvases)
        {
            scaleCanvas.SetShowAnimated(true);
        }
    }
}
