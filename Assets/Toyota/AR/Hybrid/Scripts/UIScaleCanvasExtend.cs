﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScaleCanvasExtend : UIScaleCanvas
{
    [SerializeField] private UIScaleCanvasExtend parentCanvas;
    [SerializeField] private UIScaleCanvasExtend[] childCanvases;

    public void SetShowAnimated(bool value)
    {
        _SetShow(value, true);
    }
    
    public void GoToParentCanvas()
    {
        parentCanvas.SetShowAnimated(true);

        for (var i = 0; i < childCanvases.Length; i++)
        {
            childCanvases[i].SetShowAnimated(false);
        }
    }
}
