using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum Tab
{
    ENGINE,
    DIMENSION,
    CHASSIS
}

public class InformationTab : MonoBehaviour
{
    [SerializeField] private GameObject[] tabDetails = new GameObject[3];
    [SerializeField] private GameObject[] activatedButton = new GameObject[3];
    [SerializeField] private GameObject[] deactivatedButton = new GameObject[3];

    private int currentTab = 0;

    public void ActivateTab(int value)
    {
        if (value == currentTab)
            return;
        switch (value)
        {
            case 0:
                tabDetails[0].SetActive(true);
                tabDetails[1].SetActive(false);
                tabDetails[2].SetActive(false);
                
                activatedButton[0].SetActive(true);
                activatedButton[1].SetActive(false);
                activatedButton[2].SetActive(false);
                
                deactivatedButton[0].SetActive(false);
                deactivatedButton[1].SetActive(true);
                deactivatedButton[2].SetActive(true);

                currentTab = 0;
                break;
            case 1:
                tabDetails[0].SetActive(false);
                tabDetails[1].SetActive(true);
                tabDetails[2].SetActive(false);
                
                activatedButton[0].SetActive(false);
                activatedButton[1].SetActive(true);
                activatedButton[2].SetActive(false);
                
                deactivatedButton[0].SetActive(true);
                deactivatedButton[1].SetActive(false);
                deactivatedButton[2].SetActive(true);

                currentTab = 1;
                break;
            case 2:
                tabDetails[0].SetActive(false);
                tabDetails[1].SetActive(false);
                tabDetails[2].SetActive(true);
                
                activatedButton[0].SetActive(false);
                activatedButton[1].SetActive(false);
                activatedButton[2].SetActive(true);
                
                deactivatedButton[0].SetActive(true);
                deactivatedButton[1].SetActive(true);
                deactivatedButton[2].SetActive(false);

                currentTab = 2;
                break;
        }
    }
}
