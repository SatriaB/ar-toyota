﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Hellmade.Sound;

public class UIScaleCanvas : MonoBehaviour
{
    [SerializeField]
    private float defaultYSummon = 0f;
    public bool isAnimatedDefault = true;
    public bool isLockPosRotate = true;

    private float defaultX;
    private float defaultY;

    private bool isSummoning = false;
    private bool isShowing = false;
    private bool isBusy = false;

    CoroutineHandle workingCoroutine;


    [HideInInspector]
    public bool isInitiated;

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        defaultX = transform.localScale.x;
        defaultY = transform.localScale.y;

        transform.DOScaleX(0f, 0f);
        transform.DOScaleY(0f, 0f);

        isInitiated = true;
    }

    public void Summon(bool _isPlaySFX = true)
    {

        if (isSummoning)
        {
            if (workingCoroutine != null) Timing.KillCoroutines(workingCoroutine);
            isSummoning = false;
        }

        workingCoroutine = Timing.RunCoroutine(Summoning(_isPlaySFX));
    }

    public void SummonToogle(bool _isPlaySFX = true)
    {
        if (isSummoning)
        {
            if (workingCoroutine != null) Timing.KillCoroutines(workingCoroutine);
            isSummoning = false;
        }

        if (isShowing) Timing.RunCoroutine(SetShow(false, true, _isPlaySFX));
        else workingCoroutine = Timing.RunCoroutine(Summoning(_isPlaySFX));
    }

    private IEnumerator<float> Summoning(bool _isPlaySFX = true)
    {
        isSummoning = true;
        Transform _player = null;
        _player = GameObject.Find("PC Camera").transform;

        while (isBusy) yield return Timing.WaitForOneFrame;

        StartCoroutine(SetShow(false, false, _isPlaySFX));
        yield return Timing.WaitForSeconds(.1f);

        Vector3 pos = _player.position + (_player.forward * .8f) + (_player.up * -.1f);
        if (defaultYSummon != 0)
            pos.y = defaultYSummon;

        if (!isLockPosRotate)
        {
            transform.position = pos;
            transform.LookAt(_player.transform);
        }

        StartCoroutine(SetShow(true, true, _isPlaySFX));
        yield return Timing.WaitForSeconds(1f);
        isSummoning = false;
    }

    private IEnumerator<float> SetShow(bool _showValue, bool _isanimated, bool _isPlaySFX = true)
    {
        if (isShowing == _showValue) yield break;

        var _scaleY = _showValue ? defaultY : 0f;
        var _scaleX = _showValue ? defaultX : 0f;

        isBusy = true;
        
        transform.DOScaleY(_scaleY, _isanimated ? .3f : 0f);
        if (_isanimated)
            yield return Timing.WaitForSeconds(.25f);
        transform.DOScaleX(_scaleX, _isanimated ? .5f : 0f);

        isShowing = _showValue;

        isBusy = false;
    }

    public void _SetShow(bool _showValue, bool _isanimated)
    {
        StartCoroutine(SetShow(_showValue, _isanimated));
    }

    public void SetShowAnimated(bool value)
    {
        _SetShow(value, true);
    }
}
