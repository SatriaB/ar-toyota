using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class PlaceModelOnPlane : MonoBehaviour
{
    [SerializeField] private GameObject model;
    [SerializeField] private UnityEvent onModelPlaced;
    [SerializeField] private UnityEvent onReplacingModel;

    private ARRaycastManager _arRaycastManager;
    private ARPlaneManager _arPlaneManager;
    private ARSession _arSession;

    private static List<ARRaycastHit> _sARRaycastHits = new List<ARRaycastHit>();

    public GameObject spawnedObject { get; private set; }

    private bool _modelPlaced = false;
    
    // Start is called before the first frame update
    void Start()
    {
        _arRaycastManager = GetComponent<ARRaycastManager>();
        _arPlaneManager = GetComponent<ARPlaneManager>();
        
        if (model.activeInHierarchy)
            model.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(_modelPlaced)
            return;
        
        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;
        
        if (_arRaycastManager.Raycast(touchPosition, _sARRaycastHits, TrackableType.Planes))
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            var hitPose = _sARRaycastHits[0].pose;

            /*if (spawnedObject == null)
            {
                spawnedObject = Instantiate(model, hitPose.position, hitPose.rotation);
                onModelPlaced?.Invoke();
            }
            else
            {
                spawnedObject.transform.position = hitPose.position;
            }*/
            
            model.transform.position = hitPose.position;
            model.SetActive(true);
            onModelPlaced?.Invoke();
                
            _arPlaneManager.SetTrackablesActive(false);

            _modelPlaced = true;
        }
    }
    
    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.GetMouseButtonDown(0))
        {
            touchPosition = Input.mousePosition;
            return true;
        }
        
        /*if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }*/

        touchPosition = default;
        return false;
    }

    public void TryReplaceModel()
    {
        if (_modelPlaced)
        {
            model.SetActive(false);
            onReplacingModel?.Invoke();
            _modelPlaced = false;
            _arPlaneManager.SetTrackablesActive(true);
        }
    }
}
