using System;
using System.Collections.Generic;
using UnityEngine;

namespace Toyota.AR.Hybrid.Scripts
{
    public class Control : MonoBehaviour
    {
        public bool uiInteracted { get; set; } = false;
        
        [SerializeField] private GameObject[] models;
        [SerializeField] private GameObject[] particles;
        [SerializeField] private float scaleSmoother = 1f;
        [SerializeField] private float fullScale = 1f;
        [SerializeField] private float originalScale = 1f;

        [SerializeField] private Throttle throttleControl;
        
        private float m_ZoomMagnitude = 0f;
        private float m_InitialDistance = 0f;
        
        private Vector2 m_FirstTouch = Vector2.zero;
        private Vector2 m_CurrentTouch = Vector2.negativeInfinity;
        
        private Camera m_MainCamera;
        
        private Vector3 m_InitialScale = Vector3.one;
        private Vector3 m_CacheScale = Vector3.one;

        private float _previousScaleBeforeFull = 1;
        private float _defaultScale = .1f;

        private float _bufferMagnitude = 0;
        
        private Dictionary<GameObject, Vector3> _firstSpawnPositionPoints = new Dictionary<GameObject, Vector3>();

        private bool m_canResize
        {
            get;
            set;
        } = true;
        
        // Start is called before the first frame update
        void Start()
        {
            m_MainCamera = Camera.main;

            if (!throttleControl)
                throttleControl = GetComponent<Throttle>();

            //_defaultScale = models[0].transform.localScale.x;

            foreach (var model in models)
            {
                if(!_firstSpawnPositionPoints.ContainsKey(model))
                    _firstSpawnPositionPoints.Add(model, model.transform.position);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (m_canResize && !throttleControl.IsThrottled && !uiInteracted)
            {
                int count = 0;
            
                //count = Input.GetMouseButton(0) ? 2 : 0;
                count = Input.touchCount;

                Vector3 newScale = new Vector3();
                
                switch (count)
                {
                    case 0:
                        m_FirstTouch = Vector2.zero;
                        m_InitialDistance = 0f;
                        m_CurrentTouch = Vector2.negativeInfinity;
                        m_InitialScale = models[0].transform.localScale;
                        _bufferMagnitude = 0;
                        return;
                    /*case 1:
                        if (m_FirstTouch == Vector2.zero)
                        {
                            m_FirstTouch = m_MainCamera.WorldToScreenPoint(models[0].transform.position);
                            m_InitialDistance = Vector2.Distance(m_FirstTouch, 
                                #if UNITY_EDITOR
                                Input.mousePosition
                                #elif PLATFORM_ANDROID
                                Input.GetTouch(0).position
                                #endif
                                );

                            return;
                        }

                        #if UNITY_EDITOR
                            m_CurrentTouch = Input.mousePosition;
                        #elif PLATFORM_ANDROID
                            m_CurrentTouch = Input.GetTouch(0).position;
                        #endif

                        newScale = CalculateTouchScale();
                        
                        break;*/

                    case 2:
                        if (m_FirstTouch == Vector2.zero)
                        {
                            m_FirstTouch = Input.GetTouch(0).position;
                            m_InitialDistance = Vector2.Distance(m_FirstTouch, Input.GetTouch(1).position);
                            
                            //m_FirstTouch = m_MainCamera.WorldToScreenPoint(models[0].transform.position);
                            //m_InitialDistance = Vector2.Distance(m_FirstTouch, Input.mousePosition);
                            
                            m_CacheScale = models[0].transform.localScale;
                        }

                        m_CurrentTouch = Input.GetTouch(1).position;
                        //m_CurrentTouch = Input.mousePosition;
                        
                        newScale = CalculateTouchScale();
                        
                        foreach (var model in models)
                        {
                            model.transform.localScale = newScale;
                            //model.transform.localPosition = new Vector3(_firstSpawnPositionPoints[model].x, -newScale.y, _firstSpawnPositionPoints[model].z);
                            //model.transform.localScale = Vector3.one * ((m_ZoomMagnitude - m_InitialDistance) * scaleSmoother) + m_InitialScale ;
                        }
                
                        foreach (var particle in particles)
                        {
                            particle.transform.localScale = newScale;
                        }
                        break;
                }
            }
        }

        public void ResetToOriginalScale()
        {
            foreach (var model in models)
            {
                model.transform.localScale = Vector3.one * _previousScaleBeforeFull;
                model.transform.localPosition = _firstSpawnPositionPoints[model];
            }

            foreach (var particle in particles)
            {
                particle.transform.localScale = Vector3.one * _previousScaleBeforeFull;
            }
            
            m_canResize = true;
        }

        public void ResetToFullScale()
        {
            _previousScaleBeforeFull = models[0].transform.localScale.x;
            
            foreach (var model in models)
            {
                model.transform.localScale = Vector3.one * fullScale;
                model.transform.localPosition = new Vector3(_firstSpawnPositionPoints[model].x, -fullScale, _firstSpawnPositionPoints[model].z);
            }

            foreach (var particle in particles)
            {
                particle.transform.localScale = Vector3.one * fullScale;
            }
            
            m_canResize = false;
        }

        public void ResetToDefault()
        {
            m_canResize = true;
            
            foreach (var model in models)
            {
                model.transform.localScale = Vector3.one * _defaultScale;
                model.transform.localPosition = Vector3.zero;
                //model.transform.localPosition = new Vector3(_firstSpawnPositionPoints[model].x, -fullScale, _firstSpawnPositionPoints[model].z);
            }

            foreach (var particle in particles)
            {
                particle.transform.localScale = Vector3.one * _defaultScale;
            }
        }

        private void OnEnable()
        {
            foreach (var model in models)
            {
                _firstSpawnPositionPoints[model] = model.transform.position;
            }
        }

        private Vector3 CalculateTouchScale()
        {
            m_CacheScale = models[0].transform.localScale;

            m_ZoomMagnitude = Vector2.Distance(m_FirstTouch, m_CurrentTouch);

            if (_bufferMagnitude <= 0)
                _bufferMagnitude = m_ZoomMagnitude;

            if (Mathf.Abs(m_ZoomMagnitude - _bufferMagnitude) < 0.1f)
                return m_CacheScale;

            _bufferMagnitude = m_ZoomMagnitude;
            Vector3 newScale = Vector3.Lerp(m_CacheScale, Vector3.one * (m_ZoomMagnitude - m_InitialDistance), scaleSmoother * Time.deltaTime);

            if (newScale.x >= fullScale)
                newScale = Vector3.one * fullScale;
                        
            else if (newScale.x <= originalScale)
            {
                newScale = Vector3.one * originalScale;
            }

            return newScale;
        }
    }
}

