using UnityEngine;

namespace Toyota.DatabaseIntegration
{
    [CreateAssetMenu(fileName = "Integration Setting", menuName = "Toyota/Integration Setting")]
    public class IntegrationSetting : ScriptableObject
    {
        public string baseUrl;
        public string loginHandle;
        public string registerHandle;
        public string getSelfHandle;
        public string scoreHandle;
    }
}