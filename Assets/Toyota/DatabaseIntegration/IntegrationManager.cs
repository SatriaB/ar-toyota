using UnityEngine;
using UnityEngine.Networking;
using Tictech.Loading;
using Tictech.ResourceManager;
using Tictech.TextDebugger;

namespace Toyota.DatabaseIntegration
{
    public class IntegrationManager : MonoBehaviour
    {
        public static IntegrationManager instance;

        [System.Serializable]
        public struct AuthForm
        {
            public string username;
            public string email;
            public string identifier;
            public string password;

            public AuthForm(string id, string pw)
            {
                username = "";
                email = "";
                identifier = id;
                password = pw;
            }
        }

        [Header("Integration")]
        public IntegrationSetting setting;

        #region PRIVATE VARIABLE
        public AuthData _authData;
        #endregion

        #region GETTER & SETTER
        public static AuthData Auth { get => instance._authData; }
        public static string UserToken { get => instance._authData.jwt; }
        public static bool IsAuthenticated { get => !string.IsNullOrEmpty(instance._authData.jwt); }
        #endregion

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        #region AUTH
        public static LoadingProcess RegisterUser(string json)
        {
            Debugger.Track("Register", "Registering...");
            var Regis = UnityWebRequest.Put(instance.setting.baseUrl + instance.setting.registerHandle, ToStringBytes(json));
            Regis.method = UnityWebRequest.kHttpVerbPOST;
            SetContentTypeJson(Regis);

            LoadingManager.StartLoading("Regis", Regis);
            var ldRegis = LoadingManager.GetLoading("Regis");
            ldRegis.onFinished += delegate
            {
                if (ldRegis.webRequest.result == UnityWebRequest.Result.ConnectionError)
                {
                    Debug.LogWarning(ldRegis.webRequest.error);
                }
                else
                {
                    Debug.Log(ldRegis.webRequest.downloadHandler.text);
                    instance._authData = JsonUtility.FromJson<AuthData>(ldRegis.webRequest.downloadHandler.text);
                    Debugger.Track("auth", "User authenticated, username: " + instance._authData.user.username);
                }
            };
            return ldRegis;
        }

        public static LoadingProcess LoginUser(string email, string password)
        {
            Debug.Log("Authenticating, url: " + instance.setting.baseUrl + instance.setting.loginHandle);
            Debugger.Track("auth", "Authenticating...");
            var login = UnityWebRequest.Put(instance.setting.baseUrl + instance.setting.loginHandle, ToJsonBytes(new AuthForm(email, password)));
            login.method = UnityWebRequest.kHttpVerbPOST;
            SetContentTypeJson(login);

            if (!LoadingManager.StartLoading("login", login))
            {
                Debugger.TrackWarning("auth", "Failed to start login process..");
            }

            var ldLogin = LoadingManager.GetLoading("login");
            ldLogin.onFinished += delegate
            {
                if (ResourceManager.IsRequestError(ldLogin.webRequest))
                {
                    Debug.LogWarning(ldLogin.webRequest.error);
                }
                else if (!string.IsNullOrEmpty(ldLogin.webRequest.downloadHandler.text))
                {
                    Debug.Log(ldLogin.webRequest.downloadHandler.text);
                    instance._authData = JsonUtility.FromJson<AuthData>(ldLogin.webRequest.downloadHandler.text);
                    Debugger.Track("auth", "User authenticated, username: " + instance._authData.user.username);
                }
                else
                {
                    Debug.LogError("Failed to login, empty load");
                }
            };
            return ldLogin;
        }
        #endregion

        #region PUBLIC LOADING PROCESS
        public static LoadingProcess GetScore(int id)
        {
            Debug.LogWarning("Get score method not implemented.");
            return null;
        }
        #endregion

        #region UTILITY
        public static byte[] ToJsonBytes(object target)
        {
            return System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(target));
        }

        public static byte[] ToStringBytes(string target)
        {
            return System.Text.Encoding.UTF8.GetBytes(target);
        }

        public static void SetContentTypeJson(UnityWebRequest request)
        {
            request.SetRequestHeader("content-type", "application/json; charset=utf-8");
        }

        public static void Authenticate(UnityWebRequest request)
        {
            if (IsAuthenticated)
                request.SetRequestHeader("Authorization", "Bearer " + instance._authData.jwt);
        }

        public LoadingProcess DebugLogin(string email, string password)
        {
            return LoginUser(email, password);
        }
        #endregion
    }

    [System.Serializable]
    public struct AuthData
    {
        public string jwt;
        public UserData user;
    }
}