using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Toyota.DatabaseIntegration
{
    public class IntegrationHelper : MonoBehaviour
    {
#if UNITY_EDITOR
        [Header("Debug")]
        public string debugEmail;
        public string debugPassword;
        public bool debugAuthenticate;

        private IEnumerator Start()
        {
            #region DEBUG
            if (!debugAuthenticate)
                yield break;
            IntegrationManager.instance.DebugLogin(debugEmail, debugPassword);
            #endregion
        }
#endif
    }
}