namespace Toyota.DatabaseIntegration
{
    [System.Serializable]
    public class UserData
    {
        public int id;
        public string username;
        public string email;
    }
}