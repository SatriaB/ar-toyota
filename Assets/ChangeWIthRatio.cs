using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ChangeWIthRatio : MonoBehaviour
{
    [SerializeField] float aspectRatio = 1.777777777777778f;
    [SerializeField] UnityEvent onBelow, onAbove;
    // Start is called before the first frame update
    void Start()
    {
        float currentRatio = (float)Screen.width / (float)Screen.height;
        //Debug.Log("Aspect Ratio = " + currentRatio);
        if (currentRatio >= aspectRatio)
            onAbove.Invoke();
        else onBelow.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
