using MEC;
using System.Collections;
using System.Collections.Generic;
using Tictech.EnterpriseUniversity;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PostScore : MonoBehaviour
{
    EuRuntimeManager eu;
    private void Start()
    {
        
    }
    private void FixedUpdate()
    {
        if (EuRuntimeManager.Instance)
        {
            eu = EuRuntimeManager.Instance;
            eu.WriteLog("Update : " + Time.realtimeSinceStartup);
            if (SceneManager.GetActiveScene().name != eu.currentSceneName)
            {
                if (eu.guestUser || eu.User == null)
                    return;
                eu.WriteLog("Loading scene : " + SceneManager.GetActiveScene().path);
                string sceneName = SceneManager.GetActiveScene().name;
                if (eu.scores.Exists(x => x.sceneName == sceneName))
                    Timing.RunCoroutine(eu.PostScore(eu.scores.Find(x => x.sceneName == sceneName).id, 100, ""));
                eu.currentSceneName = SceneManager.GetActiveScene().name;
            }
        }
    }
}
