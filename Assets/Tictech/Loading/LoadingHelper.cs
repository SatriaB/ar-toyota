using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Tictech.Loading
{
    public class LoadingHelper : MonoBehaviour
    {
        public GameObject loadingPanel;
        public Image loadingBar;
        public TextMeshProUGUI loadingMessage;

        public bool ProgressToMessage { get; set; }

        private string _message;
        private LoadingProcess _process;
        private CanvasGroup _cg;
        private Coroutine _fading;

        private void Awake()
        {
            _cg = loadingPanel.GetComponent<CanvasGroup>();
            if(!_cg)
                _cg = loadingPanel.AddComponent<CanvasGroup>();
            SetVisible(false);
        }

        public void Track(LoadingProcess process)
        {
            if(process == null)
            {
                SetVisible(false);
                return;
            }

            SetMessage("");
            UpdateProgress(0);
            SetVisible(true);
            _process = process;
            _process.onProgressNormalized += UpdateProgress;
            _process.onFinished += () =>
            {
                ProgressToMessage = false;
                SetVisible(false);
            };
        }

        public void SetMessage(string message)
        {
            _message = message;
            loadingMessage.text = _message;
        }

        private void UpdateProgress(float p)
        {
            loadingBar.fillAmount = p;
            if (ProgressToMessage)
                loadingMessage.text = _message + " (" + _process.NormalizedProgress.ToString("P") + ")";
        }

        public void SetVisible(bool value)
        {
            if (_cg.interactable != value)
            {
                _cg.interactable = value;
                if(_fading != null)
                    StopCoroutine(_fading);
                _fading = StartCoroutine(Fade(value));
            }
        }

        private IEnumerator Fade(bool visible)
        {
            _cg.blocksRaycasts = visible;
            _cg.interactable = visible;
            float a = 0;
            float from = _cg.alpha;
            float to = visible ? 1f : 0f;
            while(a < 1f)
            {
                a += Time.deltaTime / .2f;
                _cg.alpha = Mathf.Lerp(from, to, a);
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
