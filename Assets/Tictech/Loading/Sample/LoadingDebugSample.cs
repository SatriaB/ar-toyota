using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tictech.Loading.Sample
{
    using TextDebugger;

    public class LoadingDebugSample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            int count = Random.Range(8, 32);
            for (int i = 0; i <= count; i++)
            {
                string key = "Loading #" + i;
                StartCoroutine(LoadingText(key));
            }
            StartCoroutine(LoadingWithProgress());
        }

        private IEnumerator LoadingText(string key)
        {
            yield return new WaitForSeconds(Random.Range(1f, 8f));
            LoadingManager.StartLoading(key, Random.Range(8, 256));
            yield return new WaitForEndOfFrame();
            var loading = LoadingManager.GetLoading(key);
            loading.onProgress += delegate (float p) { Debugger.Track(key, loading.NormalizedProgress.ToString("P")); };
            loading.onFinished += delegate { Debugger.Track(key, "finished."); };

            float wait = 0f;
            while (!loading.IsFinished)
            {
                wait = Mathf.MoveTowards(wait, Random.Range(0f, 0.2f), .02f);
                yield return new WaitForSeconds(wait * wait);
                loading.Progress += Random.Range(.5f, 1.5f);
            }
        }

        private IEnumerator LoadingWithProgress()
        {
            var loadHelper = FindObjectOfType<LoadingHelper>();
            LoadingManager.StartLoading("Loading progress", Random.Range(256, 1024));
            yield return new WaitForEndOfFrame();
            var loading = LoadingManager.GetLoading("Loading progress");
            loadHelper.Track(loading);
            loadHelper.ProgressToMessage = true;
            loadHelper.SetMessage("Loading...");

            float wait = Random.Range(0f, 0.4f);
            while (!loading.IsFinished)
            {
                wait = Mathf.MoveTowards(wait, Random.Range(0f, 0.3f), .04f);
                yield return new WaitForSeconds(wait * wait);
                loading.Progress += Random.Range(.5f, 1.5f);
            }
        }
    }
}