using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Tictech.Utilities
{
    using ResourceManager;
    public class VersionChecker : MonoBehaviour
    {
        public AssetReference textRef;
        public Transform root;

        // Start is called before the first frame update
        void Start()
        {
            ResourceManager.LoadAddressable(textRef, delegate ()
            {
                textRef.InstantiateAsync(root).Completed += (async) =>
                {
                    Debug.Log(async.Result.name);
                    ResourceManager.UnloadResource(textRef.AssetGUID, true);
                    Destroy(this);
                };
            });
        }
    }
}