using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckName : MonoBehaviour
{
    public string tag = "AR";
    public UnityEvent onFound, onGone;
    void Start()
    {
        Timing.RunCoroutine(Update());
    }

    // Update is called once per frame
    IEnumerator<float> Update()
    {
        while (true)
        {
            yield return Timing.WaitForSeconds(.1f);
            if (GameObject.FindGameObjectWithTag(tag))
            {
                onFound.Invoke();
                break;
            }
        }
    }
}
